#include <cstdio>
#include <cstring>
#include <iostream>

using namespace std;

const int maxn = 5;
const int maxm = 10;

string S;
char cx[maxm], cy[maxm];
int n, d, m, x[maxm], y[maxm];

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	cin >> n >> d;
	if (n != 2 && n != 5)
	{
		cout << -1 << endl;
		return 0;
	}
	cin >> S;
	cin >> m;
	for (int i = 0; i < m; ++i)
	{
		cin >> x[i] >> cx[i] >> y[i] >> cy[i];
		--x[i]; --y[i];
	}

	if (n == 2)
	{
		for (char i = 'A'; i <= 'C'; ++i)
			if (S[0] != (char)(i-'A'+'a'))
			for (char j = 'A'; j <= 'C'; ++j)
				if (S[1] != (char)(j-'A'+'a'))
				{
					string ans = "";
					ans.push_back(i);
					ans.push_back(j);
					bool flag = true;
					for (int t = 0; t < m; ++t)
						if (ans[x[t]] == cx[t] && ans[y[t]] != cy[t])
						{
							flag = false;
							break;
						}
					if (flag)
					{
						cout << ans << endl;
						return 0;
					}
				}
		cout << "-1" << endl;
	}
	else
	{
		for (char i = 'A'; i <= 'C'; ++i)
			if (S[0] != (char)(i-'A'+'a'))
			for (char j = 'A'; j <= 'C'; ++j)
				if (S[1] != (char)(j-'A'+'a'))
				for (char k = 'A'; k <= 'C'; ++k)
					if (S[2] != (char)(k-'A'+'a'))
					for (char p = 'A'; p <= 'C'; ++p)
						if (S[3] != (char)(p-'A'+'a'))
						for (char q = 'A'; q <= 'C'; ++q)
							if (S[4] != (char)(q-'A'+'a'))
							{
								string ans = "";
								ans.push_back(i);
								ans.push_back(j);
								ans.push_back(k);
								ans.push_back(p);
								ans.push_back(q);
								bool flag = true;
								for (int t = 0; t < m; ++t)
									if (ans[x[t]] == cx[t] && ans[y[t]] != cy[t])
									{
										flag = false;
										break;
									}
								if (flag)
								{
									cout << ans << endl;
									return 0;
								}
							}
		cout << "-1" << endl;
	}

	fclose(stdin);
	fclose(stdout);

	return 0;
}