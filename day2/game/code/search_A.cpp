#include <cstdio>

const int maxm = 100000;
const int maxn = 50000;

int n, d, m, x[maxm], y[maxm];
char S[maxn], ans[maxn], cx[maxm], cy[maxm];

void input()
{
	scanf("%d%d", &n, &d);
	scanf("%s", S);
	scanf("%d", &m);
	for (int i = 0; i < m; ++i)
	{
		char s1, s2;
		scanf("%d%c%c%d%c%c", &x[i], &s1, &cx[i], &y[i], &s2, &cy[i]);
		--x[i]; --y[i];
	}
}

bool check()
{
	for (int i = 0; i < m; ++i)
		if (ans[x[i]] == cx[i] && ans[y[i]] != cy[i]) return false;
	return true;
}

bool dfs(int k)
{
	if (k >= n)
		return check();
	for (char ch = 'A'; ch <= 'C'; ++ch)
		if (S[k] != (char)(ch - 'A' + 'a'))
		{
			ans[k] = ch;
			if (dfs(k+1)) return true;
		}
	return false;
}

void solve()
{
	if (!dfs(0))
		printf("-1\n");
	else
	{
		for (int i = 0; i < n; ++i)
			printf("%c", ans[i]);
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	input();
	solve();

	fclose(stdin);
	fclose(stdout);

	return 0;
}