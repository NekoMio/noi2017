#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn = 50000;
const int maxm = 100000;
const int maxc = 3;

struct Edge_t
{
	int r, next;
};

struct Edge_t e[maxm*2];

struct Link_t
{
	int key;
	Link_t *left, *right, *next;
	Link_t() : key(-1), left(this), right(this), next(NULL) {}
	void remove()
	{
		right->left = left;
		left->right = right;
	}
	void resume()
	{
		right->left = this;
		left->right = this;
	}
	bool empty()
	{
		return right == this;
	}
	void insert(Link_t *node)
	{
		node->right = right;
		right->left = node;
		node->left = this;
		right = node;
	}
	Link_t* pop()
	{
		Link_t* ret = right;
		ret->remove();
		return ret;
	}
};

struct Link_t Link[maxc+1];
struct Link_t pool[maxn*4];
struct Link_t *Node[maxn];

bool valid[maxn];
int n, d, m, x[maxm], y[maxm];
int used[maxn][maxc], linkId[maxn];
int line, head[maxn], tmpArray[maxm];
char S[maxn], ans[maxn], cx[maxm], cy[maxm];

void initPool()
{
	for (int i = 1; i < maxn*4; ++i)
		pool[0].insert(&pool[i]);
}

Link_t* newNode()
{
	return pool[0].pop();
}

void recycle(Link_t* node)
{
	pool[0].insert(node);
}

void addEdge(int x, int y)
{
	e[line].r = y;
	e[line].next = head[x];
	head[x] = line++;
}

void input()
{
	scanf("%d%d", &n, &d);
	scanf("%s", S);
	scanf("%d", &m);
	for (int i = 0; i < m; ++i)
	{
		char s1, s2;
		scanf("%d%c%c%d%c%c", &x[i], &s1, &cx[i], &y[i], &s2, &cy[i]);
		--x[i]; --y[i];
	}
}

void ready()
{
	initPool();
	memset(used, 0, sizeof(used));
	for (int i = 0; i < n; ++i)
		if (S[i] != 'x') used[i][S[i]-'a'] = 1;
	for (int i = 0; i < n; ++i)
	{
		int s = 0;
		for (int j = 0; j < maxc; ++j)
			if (used[i][j] == 0) ++s;
		linkId[i] = s;
		Node[i] = newNode();
		Link[s].insert(Node[i]);
		Node[i]->key = i;
		valid[i] = true;
	}

	line = 0;
	memset(head, 0xff, sizeof(head));
	for (int i = 0; i < m; ++i)
		addEdge(x[i], i);
}

bool dfs()
{
	if (!Link[0].empty()) return false;
	int p;
	for (p = 1; p <= maxc; ++p)
		if (!Link[p].empty()) break;
	if (p > maxc) return true;
	Link_t* keyNode = Link[p].pop();
	int key = keyNode->key;
	valid[key] = false;
	for (int i = 0; i < maxc; ++i)
		if (used[key][i] == 0)
		{
			bool flag = true;
			ans[key] = (char)('A' + i);
			for (int j = head[key]; j != -1; j = e[j].next)
				if (ans[key] == cx[e[j].r])
				{
					int q = e[j].r;
					int r = y[q];
					if (valid[r])
						for (int c = 0; c < maxc; ++c)
							if (c != cy[q])
							{
								if (used[r][c] == 0)
								{
									--linkId[r];
									Node[r]->remove();
									Link_t* tmp = newNode();
									tmp->key = Node[r]->key;
									tmp->next = Node[r];
									Link[linkId[r]].insert(tmp);
									Node[r] = tmp;
								}
								++used[r][c];
							}
					else
						flag = flag && (ans[r] == cy[q]);
				}
			if (flag && dfs()) return true;
			int len = 0;
			for (int j = head[key]; j != -1; j = e[j].next)
				tmpArray[len++] = e[j].r;
			for (int j = len-1, q = tmpArray[j]; j >= 0; q = tmpArray[--j])
				if (ans[key] == cx[q] && valid[y[q]])
				{
					int r = y[q];
					for (int c = 0; c < maxc; ++c)
						if (c != cy[q])
						{
							--used[r][c];
							if (used[r][c] == 0)
							{
								Link_t* tmp = Node[r];
								tmp->remove();
								recycle(tmp);
								tmp = tmp->next;
								tmp->resume();
								Node[r] = tmp;
								++linkId[r];
							}
						}
				}
		}
	valid[key] = true;
	keyNode->resume();
	return false;
}

void solve()
{
	if (!dfs())
		printf("-1\n");
	else
	{
		for (int i = 0; i < n; ++i)
			printf("%c", ans[i]);
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	input();
	ready();
	solve();

	fclose(stdin);
	fclose(stdout);

	return 0;
}
