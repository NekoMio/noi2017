#include <cstdio>
#include <set>
#include <queue>
#include <vector>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

const int maxm = 100000*2;
const int maxn = 50000*2;
const int maxc = 3;
const int maxd = 8;

struct Edge_t
{
	int x, y, cx, cy;
};

struct Edge_t edge[maxm];

struct E_t
{
	int r, next;
};

struct E_t e[maxm];
struct E_t E[maxm];

queue<int> que, Q;
vector<int> xEdge[maxd];
set< pair<int,int> > edgeSet;

bool instack[maxn], flag;
char S[maxn], color[maxn];
int group[maxn], inDegree[maxn];
int n, d, m, newId[maxn], tim, vis[maxn];
int NofNode, nodeId[maxn][maxc], group_cnt;
int top, stack[maxn], depth, dfn[maxn], low[maxn];
int line, head[maxn], Line, Head[maxn], Link[maxn], Next[maxn];

void input()
{
	scanf("%d%d",&n, &d);
	scanf("%s", S);
	scanf("%d", &m);
	for (int i = 0; i < m; ++i)
	{
		int x, y;
		char cx, cy, s1, s2;
		scanf("%d%c%c%d%c%c", &x, &s1, &cx, &y, &s2, &cy);
		edge[i].x = x-1;
		edge[i].y = y-1;
		edge[i].cx = (int)(cx-'A');
		edge[i].cy = (int)(cy-'A');
	}
}

void addEdge(int x, int y)
{
	e[line].r = y;
	e[line].next = head[x];
	head[x] = line++;
}

void delEdge(int x, int y)
{
	head[x] = e[--line].next;
}

int another(int x, int j)
{
	for (int i = 0; i < maxc; ++i)
		if (i != j && nodeId[x][i] != -1)
			return nodeId[x][i];
}

void AddEdge(int x, int cx, int y, int cy)
{
	if (nodeId[x][cx] != -1)
	{
		if (nodeId[y][cy] == -1)
			addEdge(nodeId[x][cx], another(x, cx));
		else
		{
			addEdge(nodeId[x][cx], nodeId[y][cy]);
			if (x != y) addEdge(another(y, cy), another(x, cx));
		}
	}
}

void DelEdge(int x, int cx, int y, int cy)
{
	if (nodeId[x][cx] != -1)
	{
		if (nodeId[y][cy] == -1)
			delEdge(nodeId[x][cx], another(x, cx));
		else
		{
			if (x != y) delEdge(another(y, cy), another(x, cx));
			delEdge(nodeId[x][cx], nodeId[y][cy]);
		}
	}
}

void ready()
{
	int d_ct = 0;
	for (int i = 0; i < n; ++i)
		if (S[i] == 'x')
		{
			newId[i] = n-d+d_ct;
			++d_ct;
		}
		else
		{
			newId[i] = i-d_ct;
			S[i-d_ct] = S[i];
		}
	for (int i = n-d; i < n; ++i)
		S[i] = 'x';
	for (int i = 0; i < m; ++i)
	{
		edge[i].x = newId[edge[i].x];
		edge[i].y = newId[edge[i].y];
	}

	NofNode = 0;
	for (int i = 0; i < n-d; ++i)
		for (int j = 0; j < maxc; ++j)
			nodeId[i][j] = (S[i] == (char)('a'+j))? -1 : NofNode++;

	line = 0;
	memset(head, 0xff, sizeof(head));
	for (int i = 0; i < m; ++i)
		if (S[edge[i].x] == 'x' || S[edge[i].y] == 'x')
		{
			int tmp;
			if (S[edge[i].x] == 'x' && S[edge[i].y] == 'x')
				tmp = max(edge[i].x, edge[i].y);
			else
				tmp = (S[edge[i].x] == 'x')? edge[i].x : edge[i].y;
			xEdge[tmp-(n-d)].push_back(i);
		}
		else
			AddEdge(edge[i].x, edge[i].cx, edge[i].y, edge[i].cy);
}

void extendGraph(int x)
{
	int id = x-(n-d);
	for (int i = 0; i < xEdge[id].size(); ++i)
	{
		int j = xEdge[id][i];
		AddEdge(edge[j].x, edge[j].cx, edge[j].y, edge[j].cy);
	}
}

void resumeGraph(int x)
{
	int id = x-(n-d);
	for (int i = xEdge[id].size()-1; i >= 0; --i)
	{
		int j = xEdge[id][i];
		DelEdge(edge[j].x, edge[j].cx, edge[j].y, edge[j].cy);
	}
}

void tarjan(int u, int d)
{
//	cerr << d << endl;
	vis[u] = tim;
	stack[++top] = u;
	instack[u] = true;
	dfn[u] = low[u] = depth++;
	for (int i = head[u]; i != -1; i = e[i].next)
		if (vis[e[i].r] != tim)
		{
			tarjan(e[i].r, d+1);
			if (!flag) return;
			low[u] = min(low[u], low[e[i].r]);
		}
		else
		{
			if (instack[e[i].r])
				low[u] = min(low[u], dfn[e[i].r]);
		}
	if (low[u] == dfn[u])
	{
		Link[group_cnt] = -1;
		while (top)
		{
			int x = stack[top--];
			Next[x] = Link[group_cnt];
			Link[group_cnt] = x;
			group[x] = group_cnt;
			instack[x] = false;
			if (vis[x^1] == tim && instack[x^1] && dfn[x^1] >= dfn[u])
			{
				flag = false; return;
			}
			if (x == u) break;
		}
		++group_cnt;
	}
}

bool check()
{
	++tim;
	flag = true;
	group_cnt = top = depth = 0;
	for (int i = 0; i < NofNode && flag; ++i)
		if (vis[i] != tim) tarjan(i, 0);
	return flag;
}

bool dfs(int i)
{
	if (!check()) return false;
	if (i >= n) return true;
	for (char ch = 'a'; ch <= 'c'; ++ch)
	{
		S[i] = ch;
		for (int j = 0; j < maxc; ++j)
			nodeId[i][j] = (S[i] == (char)('a'+j))? -1 : NofNode++;
		extendGraph(i);
		if (dfs(i+1)) return true;
		resumeGraph(i);
		NofNode -= 2;
	}
	return false;
}

void topsort()
{
	int now, p;

	while (!que.empty())
	{
		now = que.front();
		que.pop();
		if (color[now] != 0) continue;
		color[now] = 'R';
		inDegree[now] = -1;

		for (int x = Link[now]; x != -1; x = Next[x])
		{
			Q.push(group[x^1]);
			while(!Q.empty())
			{
				p = Q.front();
				Q.pop();
				if (color[p] == 'B') continue;
				color[p]='B';
				for (int j = Head[p]; j != -1; j = E[j].next)
					Q.push(E[j].r);
			}
		}
		for(int i = Head[now]; i != -1; i = E[i].next)
			if (--inDegree[E[i].r] == 0)
				que.push(E[i].r);
	}
}

void solve()
{
	tim = 0;
	memset(vis, 0, sizeof(vis));

	if (!dfs(n-d))
		printf("-1\n");
	else
	{
		Line = 0;
		memset(Head, 0xff, sizeof(Head));
		memset(inDegree, 0, sizeof(inDegree));
		edgeSet.clear();
		for (int i = 0; i < NofNode; ++i)
			for (int j = head[i]; j != -1; j = e[j].next)
				if (edgeSet.find(make_pair(group[e[j].r], group[i])) == edgeSet.end())
				{
					if (group[i] == group[e[j].r]) continue;
					E[Line].r = group[i];
					E[Line].next = Head[group[e[j].r]];
					Head[group[e[j].r]] = Line++;
					edgeSet.insert(make_pair(group[e[j].r], group[i]));
					++inDegree[group[i]];
				}

		memset(color, 0, sizeof(color));
		for (int i = 0; i < group_cnt; ++i)
			if (inDegree[i] == 0) que.push(i);
		topsort();

		for (int i = 0; i < n; ++i)
			for (int j = 0; j < maxc; ++j)
				if (nodeId[newId[i]][j] != -1 && color[group[nodeId[newId[i]][j]]] == 'R')
					printf("%c", (char)('A'+j));
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	input();
	ready();
	solve();

	fclose(stdin);
	fclose(stdout);

	return 0;
}