#include<assert.h>
#include<cstdio>
#include<cstring>
#define MAXN 200000
#define MAXM 500000
#define MAXK 50
#define MAXSTR 10000000
int n,d,c=1,xc=1,num,m;
char s[50010];
int read_int(int min_lim,int max_lim)
{
	int x;
	scanf("%d",&x);
	assert(min_lim<=x);
	assert(x<=max_lim);
	return x;
}
void read_string(int min_lim,int max_lim)
{
	scanf("%s",s);
	int len=strlen(s),i,cnt=0;
	assert(min_lim<=len);
	assert(len<=max_lim);
	for(i=0;i<len;++i)
	{
		assert('a'==s[i]||'b'==s[i]||'c'==s[i]||'x'==s[i]);
		if(s[i]=='x')
			++cnt;
		if(s[i]!='c')
			c=0;
		if(s[i]!='x'&&s[i]!='c')
			xc=0;
	}
	assert(cnt==d);
}
void read_char()
{
	char w[10];
	scanf("%s",w);
	assert('A'<=w[0]&&w[0]<='C');
}
int main()
{
	int a,b;
	char aa,bb;
	n=read_int(1,50000);
	d=read_int(0,8);
	read_string(n,n);
	m=read_int(0,100000);
	for(int i=1;i<=m;++i)
	{
		a=read_int(1,n);
		read_char();
		b=read_int(1,n);
		read_char();
	}
	printf("%d\t%d\t%d\t%d\n",n,d,m,xc);
}
