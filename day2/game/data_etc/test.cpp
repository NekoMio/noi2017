#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <sys/time.h>

using namespace std;

string ItoS(int x)
{
	string y;
	stringstream f;
	f << x; f >> y;
	return y;
}

int run(string command)
{
	return system(command.c_str());
}

long long getTime()
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv,&tz);
	return tv.tv_sec*1000000 + tv.tv_usec;
}

int main(int argc, char **argv)
{
	string only_c = "";

	string dataCode = "/home/lch/noi2017/day2/game/data/construct";
	string stdCode = "/home/lch/noi2017/day2/game/code/strongest";
	string ansCode = "/home/lch/noi2017/day2/game/code/tarjan";

	string inFile = "test.in";
	string bestIn = "2.in";
	string stdFile = "test.std";
	string ansFile = "test.ans";
	int n = 100;
	int d = 8;

	string ans;
	long long timeStamp;
	int ct = 0, maxTime = 0, tmp = 0;
	do
	{
		run(dataCode + " " + inFile + " " + ItoS(n) + " " + ItoS(d) + " " + only_c);

		timeStamp = getTime();
		run("time " + stdCode + " " + inFile + " " + stdFile);
		timeStamp = getTime() - timeStamp;

		run("time " + ansCode + " " + inFile + " " + ansFile);
		tmp = run("./judge " + inFile + " " + stdFile + " " + ansFile);
//		tmp = run("./judge " + inFile + " " + stdFile);

		ifstream fin(ansFile.c_str());
		fin >> ans;
		if (ans != "-1")
		{
			cerr << "ct = " << ++ct << endl;
			if (timeStamp > maxTime)
			{
				maxTime = timeStamp;
				run("cp " + inFile + " " + bestIn);
				cerr << "new best input, time = " << maxTime << endl;
			}
		}
	} while (tmp == 0 && ct < 1);
	if (tmp != 0)
		cerr << "Error" << endl;

	return 0;
}