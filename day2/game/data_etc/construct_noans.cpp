#include <ctime>
#include <cstdio>
#include <cstdlib>
using namespace std;

const int maxn = 50000;

char S[maxn];

int main(int argc, char **argv)
{
	srand((unsigned)time(NULL));

	freopen(argv[1], "w", stdout);
	int N = atoi(argv[2]);
	int K = atoi(argv[3]);

	int d = 8;
	int n = 3*K*d+d;
	int R = N-n;
	printf("%d %d\n", N, d);
	for (int i = 0 ; i < R; ++i)
	{
		S[i] = (char)('a' + rand()%3);
		printf("%c", S[i]);	
	}
	for (int i = R; i < N-d; ++i)
	{
		S[i] = (char)('a'+(i%3));
		printf("%c", S[i]);
	}
	for (int i = N-d; i < N; ++i)
	{
		S[i] = 'x';
		printf("%c", S[i]);
	}
	printf("\n");

	int m = 4*K*d+13;
	int M = N;
	printf("%d\n", M);
	printf("%d %d 1\n", N-d+0+1, N-d+1+1);
	printf("%d %d 1\n", N-d+0+1, N-d+2+1);
	printf("%d %d 1\n", N-d+1+1, N-d+2+1);
	printf("%d %d 1\n", N-d+1+1, N-d+3+1);
	printf("%d %d 1\n", N-d+1+1, N-d+4+1);
	printf("%d %d 1\n", N-d+3+1, N-d+4+1);
	printf("%d %d 1\n", N-d+2+1, N-d+4+1);
	printf("%d %d 1\n", N-d+2+1, N-d+5+1);
	printf("%d %d 1\n", N-d+4+1, N-d+5+1);
	printf("%d %d 1\n", N-d+3+1, N-d+6+1);
	printf("%d %d 1\n", N-d+4+1, N-d+6+1);
	printf("%d %d 1\n", N-d+6+1, N-d+7+1);
	printf("%d %d 1\n", N-d+4+1, N-d+7+1);
// no solution
//	printf("%d %d 1\n", N-d+2+1, N-d+3+1);

	for (int i = 0; i < d; ++i)
	{
		int tmp;
		if (i == 1 || i == 5 || i == 6)
			tmp = 0;
		else if (i == 2 || i == 3 || i == 7)
			tmp = 1;
		else
			tmp = 2;
		for (int j = 0; j < K; ++j)
		{
			printf("%d %d 1\n", R+1+(i*K+j)*3+0, R+1+(i*K+j)*3+1);
			printf("%d %d 1\n", R+1+(i*K+j)*3+1, R+1+(i*K+j)*3+2);
			printf("%d %d 1\n", R+1+(i*K+j)*3+2, R+1+(i*K+j)*3+0);
			printf("%d %d 1\n", R+1+(i*K+j)*3+tmp, R+1+n-d+i);
		}
	}

	for (int i = m; i < M; ++i)
	{
		int x = rand()%R;
		int y = rand()%R;
		printf("%d %d %d\n", x+1, y+1, (S[x]==S[y])? 0 : 1);
	}

	fclose(stdin);
	fclose(stdout);

	return 0;
}