#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
using namespace std;

const int maxn = 50000;

char S[maxn], A[maxn];
pair<int,char> T[maxn]; 

int main(int argc, char **argv)
{
	srand(1);
	freopen(argv[1], "w", stdout);

	int N = 50000;
	int D = 8;
	cout << N << ' ' << D << endl;
	int K = 1400;
	int d = 4;
	int n = 3*K+d;
	int R = 5000;
	for (int i = 0; i < n-d; ++i)
		S[i] = (char)('a'+i%3);
	for (int i = n-d; i < n; ++i)
		S[i] = 'x';
	for (int i = n; i < R; ++i)
	{
		int t1 = rand()%3, t2;
		do t2 = rand()%3;
		while (t1 == t2);
		A[i] = (char)('A'+t1);
		S[i] = (char)('a'+t2);
	}
	for (int i = 0; i < N-R-(D-d); ++i)
	{
		T[i].first = rand();
		T[i].second = (char)('a'+rand()%3);
	}
	for (int i = N-R-(D-d); i < N-R; ++i)
	{
		T[i].first = rand();
		T[i].second = 'x';
	}
	sort(T, T+n);
	for (int i = 0; i < N-R; ++i)
		S[R+i] = T[i].second;
	for (int i = 0; i < N; ++i)
		cout << S[i];
	cout << endl;

	for (int i = R; i < N; ++i)
		if (i-R >= (N-R)/4)
			A[i] = (S[i] == 'c')? 'B' : 'C';
		else
			A[i] = (S[i] == 'a')? 'B' : 'A';

	int M = N*2;
	cout << M << endl;
	int m = 6*K+K+9;
	for (int i = 0; i < K; ++i)
	{
		printf("%d B %d C\n", 1+3*i+0, 1+3*i+1);
		printf("%d C %d A\n", 1+3*i+0, 1+3*i+1);
		printf("%d C %d A\n", 1+3*i+1, 1+3*i+2);
		printf("%d A %d B\n", 1+3*i+1, 1+3*i+2);
		printf("%d A %d B\n", 1+3*i+2, 1+3*i+0);
		printf("%d B %d C\n", 1+3*i+2, 1+3*i+0);
		int t1 = rand()%3;
		int t2 = rand()%3, t3;
		do t3 = rand()%3;
		while (t3 == t2);
		printf("%d %c %d %c\n", 1+n-d+t1, (char)('A'+t1), 1+3*i+t2, (char)('A'+t3));
	}
	int x = 1+n-d+3;
	printf("%d %c %d %c\n", x, 'C', n-d+0, 'A');
	printf("%d %c %d %c\n", x, 'A', n-d+1, 'B');
	printf("%d %c %d %c\n", x, 'B', n-d+2, 'C');
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			if (i != j) printf("%d %c %d %c\n", n-d+i, (char)('A'+i), n-d+j, (char)('A'+j));

	for (int i = R+1; i < N; ++i)
	{
		if (i-R > (N-R)/4)
		{
			char cx = 'C', cy = 'C';
			while (A[i-1] == cx || S[i-1] == (char)(cx-'A'+'a')) --cx;
			while (A[i] == cy || S[i] == (char)(cy-'A'+'a')) --cy;
			cout << i << ' ' << cx << ' ' << i+1 << ' ' << cy << endl;
		}
		else
		{
			char cx = 'A', cy = 'A';
			while (A[i-1] == cx || S[i-1] == (char)(cx-'A'+'a')) ++cx;
			while (A[i] == cy || S[i] == (char)(cy-'A'+'a')) ++cy;
			cout << i << ' ' << cx << ' ' << i+1 << ' ' << cy << endl;	
		}
	}
	char cx = 'C';
	while (A[N-2] == cx || S[N-2] == (char)(cx-'A'+'a')) --cx;
	cout << N-1 << ' ' << cx << ' ' << N << ' ' << A[N-1] << endl;

	for (int i = m+(N-R); i < M; ++i)
	{
		int x = 1+n+rand()%(R-n), y;
		do y = 1+n+rand()%(R-n);
		while (x == y);
		printf("%d %c %d %c\n", x, A[x-1], y, A[y-1]);
	}

	fclose(stdin);
	fclose(stdout);

	return 0;
}