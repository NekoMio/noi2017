#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define STR_SIZE 10000
#define EVAL_OUTPUT_FILE "/tmp/_eval.score"

int score = 0;

FILE *fpin, *fpout, *fpans, *fpeval;
/* Output message and exit */
void output(char *s, int d)
{
	if (fpeval) {
		fprintf (fpeval, "%s\n%d\n", s, d);
		fclose(fpeval);
	}
	exit(d != 0);
}

/* Open files and check */
void open_files(char *in, char *out, char *ans, char *eval_output)
{
	if ((fpeval = fopen (eval_output, "w")) == NULL) {
		fprintf (stderr, "Can not open %s!\n", EVAL_OUTPUT_FILE);
		output ("Can not open eval record file!", 0);
	}

	if ((fpin = fopen (in, "r")) == NULL) {
		fprintf (stderr, "Can not open %s!\n", in);
		output ("Can not open standard input file!", 0);
	}

	if ((fpout = fopen (out, "r")) == NULL) {
		fprintf (stderr, "Can not open %s!\n", out);
		output ("Can not open player's output file!", 0);
	}

	if ((fpans = fopen (ans, "r")) == NULL) {
		fprintf (stderr, "Can not open %s!\n", ans);
		output ("Can not open standard answer!", 0);
	}
}

const int maxn = 50000;
char S[maxn], A[maxn];

int judge(FILE *in, FILE *ans, FILE *std){
	score = 0;
	fscanf(std, "%s", S);
	fscanf(ans, "%s", A);
	if (strcmp(S, "-1") == 0)
	{
		if (strcmp(A, "-1") == 0)
		{
			score = 5;
			return 0;
		}
		return 1;
	}
	if (strcmp(A, "-1") == 0) return 1;

	int n, d, m;
	fscanf(in, "%d%d", &n, &d);
	if (strlen(A) != n) return 1;
	fscanf(in, "%s", S);
	for (int i = 0; i < n; ++i)
	{
		if (A[i] < 'A' || 'C' < A[i]) return 1;
		if (A[i] == (char)(S[i]-'a'+'A')) return 1;
	}
	fscanf(in, "%d", &m);
	for (int i = 0; i < m; ++i)
	{
		int x, y;
		char space1, space2, cx, cy;
		fscanf(in, "%d%c%c%d%c%c", &x, &space1, &cx, &y, &space2, &cy);
		if (A[x-1] == cx && A[y-1] != cy) return 1;
	}
	score = 5;
}

int main(int argc, char **argv)
{
	if (argc != 4)
	{
		fprintf(stderr, "Usage: %s <in> <out> <ans>\n", argv[0]);
		output("Invalid Call!", 0);
	}

	open_files(argv[1], argv[2], argv[3], EVAL_OUTPUT_FILE);

	/* Compare the contens */
	judge(fpin, fpout, fpans);
	
	if (score)
		output("Right Output!!!", score);
	else
		output("Wrong Output!!!", score);

	return 0;
}
