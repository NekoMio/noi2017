#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)
#define foreach(x, y) for(__typeof(y.begin()) x = y.begin(); x != y.end(); ++x)
using namespace std;
typedef long long ll;

template <class T>
struct Alloc
{
	static vector<T *> freed;
	static T *alloc()
	{
		static const int cnt = 4096;
		if(freed.empty())
		{
			T *t = new T[cnt];
			g(i, 0, cnt)
				freed.push_back(t + i);
		}
		T *ans = freed.back();
		freed.pop_back();
		return ans;
	}
	static void dealloc(T *t)
	{
		freed.push_back(t);
	}
};
struct Node;
template <>
vector<int *> Alloc<int>::freed = vector<int *>();
template <>
vector<Node *> Alloc<Node>::freed = vector<Node *>();

template <class T>
T *alloc()
{
	return Alloc<T>::alloc();
}
template <class T>
void dealloc(T *t)
{
	Alloc<T>::dealloc(t);
}

template <class T>
struct SmartPtr
{
	T *ptr; int *cnt;
	SmartPtr(T *p = 0): ptr(p), cnt(alloc<int>())
	{
		*cnt = 1;
	}
	SmartPtr(const SmartPtr<T>& p): ptr(p.ptr), cnt(p.cnt)
	{
		++*cnt;
	}
	~SmartPtr()
	{
		dispose();
	}
	SmartPtr<T> &operator=(const SmartPtr<T>& p)
	{
		if(this != &p)
		{
			dispose();
			ptr = p.ptr;
			cnt = p.cnt;
			++*cnt;
		}
		return *this;
	}
	T& operator*() const
	{
		return *ptr;
	}
	T* operator->() const
	{
		return ptr;
	}
	void dispose()
	{
		if(ptr && !--*cnt)
		{
			dealloc(cnt);
			dealloc(ptr);
		}
	}
};

struct Node
{
	SmartPtr<Node> ls, rs;
	int mx;
};
SmartPtr<Node> newNode()
{
	return SmartPtr<Node>(alloc<Node>());
}
SmartPtr<Node> newNode(SmartPtr<Node> ls, SmartPtr<Node> rs)
{
	SmartPtr<Node> ans = newNode();
	ans->ls = ls; ans->rs = rs;
	ans->mx = max(ls->mx, rs->mx);
	return ans;
}
SmartPtr<Node> nil;
SmartPtr<Node> newNode(int mx)
{
	SmartPtr<Node> ans = newNode();
	ans->ls = ans->rs = nil;
	ans->mx = mx;
	return ans;
}

#define MaxVal 1000000007
#define MaxSeg (MaxVal * 2)

SmartPtr<Node> chg(SmartPtr<Node> root, int x, int y, int l = 0, int r = MaxSeg)
{
	if(!y)
		return root;
	if(l == r)
		return newNode(root->mx + y);
	int m = l + ((r - l) >> 1);
	if(x <= m)
		return newNode(chg(root->ls, x, y, l, m), root->rs);
	return newNode(root->ls, chg(root->rs, x, y, m + 1, r));
}
SmartPtr<Node> pop(SmartPtr<Node> root, int &ans, int l = 0, int r = MaxSeg)
{
	if(l == r)
	{
		ans = l;
		return newNode(root->mx - 1);
	}
	int m = l + ((r - l) >> 1);
	if(root->rs->mx)
		return newNode(root->ls, pop(root->rs, ans, m + 1, r));
	return newNode(pop(root->ls, ans, l, m), root->rs);
}
int gmax(SmartPtr<Node> root, int l = 0, int r = MaxSeg)
{
	if(l == r)
		return l;
	int m = l + ((r - l) >> 1);
	if(root->rs->mx)
		return gmax(root->rs, m + 1, r);
	return gmax(root->ls, l, m);
}

const int MaxDays = 100007;
const int MaxFoods = 100007;
int qd[MaxDays], mxqd = 0, qn;
SmartPtr<Node> heap[MaxDays];

int fva[MaxFoods], fvb[MaxFoods], ftot[MaxFoods], frate[MaxFoods], fn, fpd;

void buildHeaps()
{
	static vector<int> due2Food[MaxDays];
	f(i, 1, fn)
		due2Food[frate[i] ? min(mxqd, (ftot[i] + frate[i] - 1) / frate[i]) : mxqd].push_back(i);
	heap[0] = chg(nil, 0, 1000000000);
	f(i, 1, fn)
		heap[0] = chg(heap[0], fva[i], frate[i]);
	heap[1] = heap[0];
	f(i, 1, mxqd)
	{
		foreach(it, due2Food[i])
		{
			int rm = ftot[*it] - frate[*it] * (i - 1) - 1;
			heap[i] = chg(heap[i], fva[*it], rm - frate[*it]);
			heap[i] = chg(heap[i], fva[*it] + fvb[*it], 1);
		}
		heap[i + 1] = heap[i];
		foreach(it, due2Food[i])
		{
			int rm = ftot[*it] - frate[*it] * (i - 1) - 1;
			heap[i + 1] = chg(heap[i + 1], fva[*it], -rm);
			heap[i + 1] = chg(heap[i + 1], fva[*it] + fvb[*it], -1);
		}
	}
}

int prev_[MaxDays], cnt_[MaxDays];
int prev(int x)
{
	if(cnt_[x] < fpd)
		return x;
	return prev_[x] = prev(prev_[x]);
}

int tree[262144];
void chg(int x, int y)
{
	tree[x ^= 131072] = y;
	for(x >>= 1; x; x >>= 1)
		tree[x] = max(tree[x << 1], tree[x << 1 | 1]);
}
int gmax(int l, int r)
{
	int x, mx = -1;
	for(l += 131071, r += 131073; l ^ r ^ 1; l >>= 1, r >>= 1)
	{
		if(!(l & 1) && tree[l ^ 1] > mx)
		{
			mx = tree[l ^ 1]; x = l ^ 1;
		}
		if((r & 1) && tree[r ^ 1] > mx)
		{
			mx = tree[r ^ 1]; x = r ^ 1;
		}
	}
	while(x < 131072)
		if(tree[x] == tree[x << 1 | 1])
			x = (x << 1 | 1);
		else
			x <<= 1;
	return x ^ 131072;
}
void buildTree()
{
	f(i, 1, mxqd)
		chg(i, gmax(heap[i]));
}

ll ans[MaxDays];

int main()
{
	nil = newNode();
	nil->ls = nil->rs = nil; nil->mx = 0;
	
	scanf("%d%d%d", &fn, &fpd, &qn);
	f(i, 1, fn)
		scanf("%d%d%d%d", fva + i, fvb + i, ftot + i, frate + i);
	f(i, 1, qn)
	{
		scanf("%d", qd + i);
		mxqd = max(mxqd, qd[i]);
	}
	
	buildHeaps();
	buildTree();
	f(i, 1, mxqd)
		prev_[i] = i - 1;
	
	int mnd = 1;
	f(i, 1, mxqd)
	{
		ans[i] = ans[i - 1];
		f(j, 1, fpd)
		{
			int day = gmax(mnd, mxqd);
			int mx;
			heap[day] = pop(heap[day], mx);
			// printf("day %d : get %d from day %d | remaining %d\n", i, mx, day, gmax(heap[day]));
			ans[i] += mx;
			chg(day, gmax(heap[day]));
			++cnt_[prev(day)];
			while(!prev(mnd))
				chg(mnd++, -1);
		}
	}
	f(i, 1, qn)
		printf("%lld\n", ans[qd[i]]);
	return 0;
}
