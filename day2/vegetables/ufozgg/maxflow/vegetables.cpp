#include<cstdlib>
#include<cstring>
#include<cstdio>
#define INF 2147483647
#define MAXV 100000 
#define MAXE 1000000 
#define MAXN 1000000 
#define MAXK 1000000 
int n,m,k;
int a[MAXN+10],s[MAXN+10],f[MAXN+10],x[MAXN+10];
int q[MAXK+10];
long long ans[MAXK+10];
int maxp;
long long ANS;
int to[MAXE+10],next[MAXE+10],cost[MAXE+10],flow[MAXE+10],last[MAXV+10],top=1;
int dis[MAXN+10],seq[MAXN*40],head,tail;
bool inseq[MAXN+10];
int fa[MAXN+10];
void add_edge(int fr,int tt,int flo,int val)
{
	++top;
	to[top]=tt;
	flow[top]=flo;
	cost[top]=val;
	next[top]=last[fr];
	last[fr]=top;
	++top;
	to[top]=fr;
	flow[top]=0;
	cost[top]=-val;
	next[top]=last[tt];
	last[tt]=top;
}
bool spfa()
{
	memset(inseq,0,sizeof(inseq));
	memset(dis,200,sizeof(dis));
	dis[1]=0;
	head=0;
	tail=1;
	seq[1]=1;
	inseq[1]=1;
	while(head<tail)
	{
		++head;
		inseq[seq[head]]=0;
		for(int i=last[seq[head]];i;i=next[i])
			if(flow[i]&&dis[to[i]]<dis[seq[head]]+cost[i])
			{
				dis[to[i]]=dis[seq[head]]+cost[i];
				fa[to[i]]=i;
				if(!inseq[to[i]])
				{
					seq[++tail]=to[i];
					inseq[to[i]]=1;
				}
			}
	}
	if(dis[2]==dis[0])
		return false;
	for(int i=2;i!=1;i=to[fa[i]^1])
	{
		ANS+=cost[fa[i]];
		--flow[fa[i]];
		++flow[fa[i]^1];
	}
	return true;
}
void cost_flow()
{
	while(spfa());
}
int main()
{
	int i,j;
	scanf("%d%d%d",&n,&m,&k);
	for(i=1;i<=n;++i)
		scanf("%d%d%d%d",a+i,s+i,f+i,x+i);
	for(i=1;i<=k;++i)
	{
		scanf("%d",q+i);
		if(q[i]>maxp)
			maxp=q[i];
	}
	for(i=1;i<=maxp;++i)
		for(j=1;j<=n;++j)
			if(f[j]-x[j]*i>0)
				add_edge(i+2,2,x[j],a[j]);
			else
			if(f[j]-x[j]*(i-1)>0)
			{
				add_edge(i+2,2,f[j]-x[j]*(i-1)-1,a[j]);
				add_edge(i+2,2,1,a[j]+s[j]);
			}
	for(j=1;j<=n;++j)
		if(f[j]-x[j]*maxp>0)
			{
				add_edge(maxp+3,2,f[j]-x[j]*maxp-1,a[j]);
				add_edge(maxp+3,2,1,a[j]+s[j]);
			}
	for(i=1;i<=maxp;++i)
		add_edge(i+2,i+3,INF,0);
	for(i=1;i<=maxp;++i)
	{
		add_edge(1,i+2,m,0);
		cost_flow();
		ans[i]=ANS;
	}
	for(i=1;i<=k;++i)
		printf("%lld\n",ans[q[i]]);
	return 0;
}

