import os, sys
from tqdm import tqdm

if __name__ == '__main__':
	
	os.system('g++ -o bruteforce bruteforce.cpp -O2')
	
	if os.path.exists(sys.argv[1]) == False:
		os.makedirs(sys.argv[1])
	
	for i in tqdm(range(20)):
		inp = 'phantom{}.in'.format(i+1)
		ans = 'phantom{}.ans'.format(i+1)
		outp = 'output.txt'	
		os.system('./bruteforce <{} >{}'.format(os.path.join(sys.argv[1], inp), os.path.join(outp)))
		if os.system('diff {} {}'.format(outp, os.path.join(sys.argv[1], ans))):
			break
		print('correct {}'.format(i))
		
	os.system('rm -r bruteforce output.txt')

