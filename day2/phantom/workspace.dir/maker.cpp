#include <set>
#include <map>
#include <cmath>
#include <cstdio>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <assert.h>
#define MAX_M 100000
#define MAX_N 100000
#define CLOCKWISE 1
#define ANTICLOCKWISE -1
#define LEFT_CONVEX 1
#define RIGHT_CONVEX -1
#define CONSTRUCT_LAYERS 1
#define SINGLE_QUERY -1
#define MAX_LAYERS 1
#define INF (~0U>>1)
#define RANDOM 0
#define TRIANGLE 1
#define RING 2
#define TYPE_N 3
#define SMALL_N 1
#define LARGE_N 0
using namespace std;

int g_n, g_m, g_seed;

struct Point {
	int x, y;
	int idx;
	bool removed;
	bool fixed;
	int layer;
};

long long cx(Point *a, Point *b, Point *c) {
	return (long long) (b->x-a->x)*(c->y-a->y) - (long long) (b->y-a->y)*(c->x-a->x);
}

vector <Point *> rlst; 

set < pair <int, int> > pts; 
long long area;
int removed[MAX_N+10];
int idx[MAX_N+10];
int *s_idx, *p_idx;

int k;

Point *o;
Point *save_p[MAX_N+10];
Point *p[MAX_N+10], *p2[MAX_N+10], *q[MAX_N+10], *rp[MAX_N+10];

void parse_error(const char *a, char b) {
	fprintf(stderr, "parse arg %s failed. Parameter %c should be non-negative integers.\n", a, b);
	exit(0);
}
void parse_error(const char *a, const char *b) {
	fprintf(stderr, "parse arg %s failed. Parameter %s should be non-negative integers.\n", a, b);
	exit(0);
}
void parse_args(int argc, char **argv, int &n, int &m, int &k_min, int &k_max, int &seed) {
	k_min = 1;
	k_max = 0;
	seed = 1;
	char **end = argv + argc;
	char cur_v = 'n';
	while (++argv != end) {
		char *s = *argv;
		char *t;
		if ((t = strstr(s, "=")) == NULL) {
			int v = atoi(s);
			if (cur_v == '?') {
				fprintf(stderr, "parse arg %s failed. No matched parameter.\n", s);
				exit(0);
			}
			if (v <= 0) {
				parse_error(s, cur_v);
			}
			if (cur_v == 'n') {
				n = v, cur_v = 'm';
			}
			else
				m = v, cur_v = '?';
		}
		else {
			string str(s);
			str = str.substr(0, t-s);
			int v = atoi(t+1);
			if (str == "-k_min") {
				if (v <= 0) parse_error(s, str.c_str());
				k_min = v;
			}
			else if (str == "-k_max") {
				if (v <= 0) parse_error(s, str.c_str());
				k_max = v;
			}
			else if (str == "-seed") {
				if (v <= 0) parse_error(s, str.c_str());
				seed = v;
			}
			else {
				fprintf(stderr, "parse arg %s failed. Unknown arg %s.\n", s, str.c_str());
				exit(0);
			}
		}
	}
}
void bruteforce(int n, Point **p, int &t, Point **q) {
	int h;
	h = 1, t = 0;
	//cerr << n;
	for (int i = 1; i <= n; ++i) {
		if (p[i]->removed == true)
			continue;
		while (t > 1 && cx(q[t-1], q[t], p[i]) >= 0) --t;
		q[++t] = p[i];
	}
	h = t, --t;
	for (int i = n; i >= 1; --i) {
		if (p[i]->removed == true)
			continue;
		while (h < t && cx(q[t-1], q[t], p[i]) >= 0) --t;
		q[++t] = p[i];
	}
	//cerr << " " << t << endl;
	if (t <= 0) t = 0;
	if (t == 1) t = 2, q[t] = q[1];
}
bool cmp_point2d(Point *a, Point *b) {
	return a->x == b->x ? a->y < b->y : a->x < b->x;
}
void init() {
	o = new Point();
	o->x = o->y = 0;	
}
Point *new_point(int x, int y, int idx = -1) {
	if (idx == -1) {
		pair <int, int> od = make_pair(x, y);
		if (pts.find(od) == pts.end())
			pts.insert(od);
		else {
			cerr << "maker error : n=" << g_n << ", m=" << g_m << ", seed=" << g_seed << ". duplicate point (" << x << "," << y << ")." << endl;
			exit(0);
		}
	}
	Point *pt = new Point();
	pt->x = x;
	pt->y = y;
	pt->removed = false;
	pt->layer = 0;
	if (idx == 0) pt->idx = 0, pt->fixed = true;
	else {
		pt->idx = *(p_idx++);
		save_p[pt->idx] = pt;
	}
	return pt;
}
void remove(Point *a) {
	rlst.push_back(a);
	a->removed = true, --k;
}
struct Random_Field {
	Point **p;
	int n;
	void generate(Point *a, Point *b, int n, bool mode) {
		if (mode == LARGE_N) {
			//cerr << "random_field..." << endl;
			p = (Point **) malloc(sizeof(Point *) * (n+200)); 
			int nb = (int) (3.2 * sqrt((double) n));
			for (int i = 1; i <= n; ++i) { 
				int x, y;
				while (true) {
					x = (int) (a->x + (double) (b->x - a->x) * (rand() % nb) / nb);
					y = (int) (a->y + (double) (b->y - a->y) * (rand() % (nb - 1) + 1) / nb);
					if (pts.find(make_pair(x, y)) == pts.end())
						break;
				}
				p[i] = new_point(x, y);
			}
			p[n+1] = a;
			p[n+2] = b;
			p[n+3] = new_point(a->y, b->x, 0);
			this->n = n + 3;
		}
		else {
			//cerr << "random_field..." << endl;
			p = (Point **) malloc(sizeof(Point *) * (n+200));
			int bd = 0;
			if (n <= 10)
				bd = 10;
			else if (n <= 20)
				bd = 20;
			else if (n <= 50)
				bd = 30;
			else if (n <= 100)
				bd = 40;
			else if (n <= 1000)
				bd = 100;
			for (int i = 1; i <= n; ++i) { 
				int x, y;
				while (true) {
					x = rand() % bd;
					y = rand() % bd;
					if (pts.find(make_pair(x, y)) == pts.end())
						break;
				}
				p[i] = new_point(x, y);
			}
			this->n = n;
		}
	}
	void insert(Point *a, Point *b, int t) {
		int nb = (int) (3.2 * sqrt((double) n));
		for (int i = 1; i <= t; ++i) { 
			int x, y;
			while (true) {
				x = (int) (a->x + (double) (b->x - a->x) * (rand() % nb) / nb);
				y = (int) (a->y + (double) (b->y - a->y) * (rand() % (nb - 1) + 1) / nb);
				if (pts.find(make_pair(x, y)) == pts.end())
					break;
			}
			p[++n] = new_point(x, y);
		}
	}
	void sort_p() {
		sort(p+1, p+n+1,cmp_point2d);
	}
};
struct SPCaseSmmK {
	map < pair <int, int> , Point *> index;
	int m;
	vector < Point *> lst;
	
	map <Point *, Point *> left, right, nextp;
	
	Point *get_left(Point *x, Point *a = NULL) {
		if (a != NULL) {
			left[x] = a;
			return NULL;
		}
		else if (left.find(x) == left.end())
			return NULL;
		return left[x];
	}
	Point *get_right(Point *x, Point *a = NULL) {
		if (a != NULL) {
			right[x] = a;
			return NULL;
		}
		else if (right.find(x) == right.end())
			return NULL;
		return right[x];
	}
	Point *get_next(Point *x, Point *a = NULL) {
		if (a != NULL) {
			nextp[x] = a;
			return NULL;
		}
		else if (nextp.find(x) == nextp.end())
			return NULL;
		return nextp[x];
	}
	
	Point *get(int layer, int pos, Point *a = NULL) {
		pair <int, int> x = make_pair(layer, pos);
		if (a != NULL) {
			index[x] = a;
			return NULL;
		}
		else if (index.find(x) == index.end())
			return NULL;
		return index[x];
	}
	void generate(Point *a, Point *b, int n, int k) {
		int m = n / ((1<<(k+1)) - 1);
		this->m = m;
		this->lst.clear();
		
		double L = a->x;
		double ang;
		Point *p;
		for (int i = 1; i <= k+2; ++i) {
			double tnx, tny, tx, ty;
			double centx, centy;
			centx = 0, centy = 0;
			tx = -L, ty = 0;
			double pi = acos(-1.);
			double ang = pi / (m+1);
			int rk = min(n, m);
			int x, y;
			for (int j = 1; j <= rk; ++j) {
				tnx = (tx * cos(ang) - ty * sin(ang));
				tny = (tx * sin(ang) + ty * cos(ang)); 
				tx = tnx, ty = tny;
				x = centx + tnx;
				y = centy + tny;
				p = new_point(x, y);
				get(i, j, p);
				if (i == 1)
					lst.push_back(p);
				else if (j % 2 == 0)
					get_next(get(i-1, j/2), p);
				if (j != 1) {
					get_left(get(i, j-1), p);
					get_right(p, get(i, j-1));
				}
			}
			n -= rk;
			m *= 2;
			L *= cos(ang/2);
			L = floor(L - max(10., min(L * 0.03, 50.)));
			//fprintf(stderr, "%d %.10lf\n", i, L);
			//cerr << i << " " << L << endl;
		}
	}
};
struct SPCaseOne {
	int cnt;
	Point **p1;
	void generate(int n) {
		Point *a = new_point(0, (int) 1e8);
		Point *b = new_point(0, (int) -1e8);
		int na = (n - 2) / 4;
		int rst = n - 2 - (na - 1) * 2;
		int nb1 = max(1, min(rst / 10, 
		200)), nb2 = rst - nb1;
		
		//cerr << nb1 << endl;
		
		p1 = (Point **) malloc(sizeof(Point *) * (n+10));
		cnt = 0;
		
		int x, y;
		x = (a->x + b->x) / 2;
		y = (a->y + b->y) / 2;

		double pi = acos(-1.);
		double ang;
		
		ang = 0;
		double L = (int) 1e8;
		double tnx, tny, tx, ty;
		double centx, centy;
		centx = x + L * cos(ang), centy = y + L * sin(ang);
		tx = (a->x - centx);
		ty = (a->y - centy);
		Point *p;
		for (int i = 1; i < na; ++i) {
			ang = (pi / 2) / na;
			tnx = (tx * cos(ang) - ty * sin(ang));
			tny = (tx * sin(ang) + ty * cos(ang)); 
			tx = tnx, ty = tny;
			x = centx + tnx;
			y = centy + tny;
			p = new_point(x, y);
		}
		
		centx = 0, centy = 0;
		tx = a->x - centx;
		ty = a->y - centy;
	
		for (int i = 1; i < na; ++i) {
			ang = pi / na;
			tnx = (tx * cos(ang) - ty * sin(ang));
			tny = (tx * sin(ang) + ty * cos(ang)); 
			tx = tnx, ty = tny;
			x = centx + tnx;
			y = centy + tny;
			p = new_point(x, y);
			p1[++cnt] = p;
		}
		
		centx = 0, centy = 0;
		tx = b->x - centy;
		ty = b->y - centy;
		
		for (int i = 1; i <= nb1; ++i) {
			ang = pi / (nb1 + 1);
			tnx = (tx * cos(ang) - ty * sin(ang));
			tny = (tx * sin(ang) + ty * cos(ang)); 
			tx = tnx, ty = tny;
			x = centx + tnx;
			y = centy + tny;
			p = new_point(x, y);
			p1[++cnt] = p;
		}
		ang = pi / (nb1 + 1);
		
		//cerr << na << " " << nb1 << " " << nb2 << endl;
		
		centx = 0, centy = 0;
		double rate = floor(L * cos(ang/2) - max(10., min(L * 0.03, 200.))) / L;
		//cerr << rate << endl;
		tx = (b->x - centx) * rate;
		ty = (b->y - centy) * rate;
		
		//cerr << tx << " " << ty << endl;
		for (int i = 1; i <= nb2; ++i) {
			ang = pi / (nb2 + 1);
			tnx = (tx * cos(ang) - ty * sin(ang));
			tny = (tx * sin(ang) + ty * cos(ang)); 
			tx = tnx, ty = tny;
			x = centx + tnx;
			y = centy + tny;
			p = new_point(x, y);
		}
	}
};
struct Ring {
	map <Point *, Point *> lef, rig;
	map < pair <int, int> , Point *> index;
	Point *pt1, *pt2, *root;
	
	Ring() {	
		lef.clear();
		rig.clear();
		index.clear();
	}
	Point *get(int layer, int pos, Point *a = NULL) {
		pair <int, int> x = make_pair(layer, pos);
		if (a != NULL) {
			index[x] = a;
			return NULL;
		}
		else if (index.find(x) == index.end())
			return NULL;
		return index[x];
	}
	Point *get_left(Point *x, Point *a = NULL) {
		if (a != NULL) {
			lef[x] = a;
			return NULL;
		}
		else if (lef.find(x) == lef.end())
			return NULL;
		return lef[x];
	}
	Point *get_right(Point *x, Point *a = NULL) {
		if (a != NULL) {
			rig[x] = a;
			return NULL;
		}
		else if (rig.find(x) == rig.end())
			return NULL;
		return rig[x];
	}
	void generate(Point *a, Point *b, int na, int nb) {
		//cerr << "ring..." << endl;
		int x, y;
		double ang = acos(-1.) / 8;
		Point *cent = new_point(a->x, b->y, 0);
		
		int nx, ny;
		
		x = a->x - cent->x, y = a->y - cent->y;
		
		nx = (int) (x * cos(ang) - y * sin(ang));
		ny = (int) (x * sin(ang) + y * cos(ang)); 
		x = nx, y = ny;
		root = new_point(cent->x + x, cent->y + y);
		get(1, 1, root);
		
		//cerr << x << " " << y << endl;

		nx = (int) (x * cos(ang) - y * sin(ang));
		ny = (int) (x * sin(ang) + y * cos(ang)); 
		x = nx, y = ny;
		pt1 = new_point(cent->x + x, cent->y + y);

		nx = (int) (x * cos(ang) - y * sin(ang));
		ny = (int) (x * sin(ang) + y * cos(ang)); 
		x = nx, y = ny;
		pt2 = new_point(cent->x + x, cent->y + y);
		
		//cerr << nb << endl;
		
		Point *p, *p1, *p2;
	
		x = (pt1->x + b->x) / 2;
		y = (pt1->y + b->y) / 2;

		double pi = acos(-1.);
		
		ang = 0.875 * pi;
		double L = sqrt((double) (x - pt1->x) * (x - pt1->x) + (double) (y - pt1->y) * (y - pt1->y)) / tan(pi / 16);

		double centx, centy;
		centx = x + L * cos(ang), centy = y + L * sin(ang);

		double tnx, tny, tx, ty;
		
		tx = pt1->x - centx;
		ty = pt1->y - centy;
		
		for (int i = 1; i < nb; ++i) {
			ang = pi / 8 / nb;
			tnx = (tx * cos(ang) - ty * sin(ang));
			tny = (tx * sin(ang) + ty * cos(ang)); 
			tx = tnx, ty = tny;
			x = centx + tnx;
			y = centy + tny;
			p = new_point(x, y);
		}
		get(1, 0, a), get(1, 2, pt1);
		
		int c = 2;
		
		if (true) {
			while (na) {
				for (int i = 1; i <= c; ++i)
					idx[i] = i;
				//random_shuffle(idx + 1, idx + c + 1);
				int cn = min(na, c);
				for (int i = 1; i <= cn; ++i) {
					p1 = get(c - 1, idx[i]);
					p2 = get(c - 1, idx[i] - 1);
					x = (int) ceil (p1->x + (double) (p2->x - p1->x) / (c + 1) * idx[i] + 1);
					y = (int) floor(p1->y + (double) (p2->y - p1->y) / (c + 1) * idx[i] - 1);
					p = new_point(x, y);
					get_left(p1, p), get_right(p2, p);
					get(c, idx[i], p);
				}
				p = new_point(a->x + c, a->y, 0);
				get(c, 0, p);
				p = new_point(pt1->x + c, pt1->y - c, 0);
				get(c, c + 1, p);
				na -= cn;
				++c;
			}
		}
	}
};
struct Triangle {
	map <Point *, Point *> left, right, nextp;
	Point *root;

	Triangle() {
		left.clear();
		right.clear();
		nextp.clear();
	}
	Point *get_left(Point *x, Point *a = NULL) {
		if (a != NULL) {
			left[x] = a;
			return NULL;
		}
		else if (left.find(x) == left.end())
			return NULL;
		return left[x];
	}
	Point *get_right(Point *x, Point *a = NULL) {
		if (a != NULL) {
			right[x] = a;
			return NULL;
		}
		else if (right.find(x) == right.end())
			return NULL;
		return right[x];
	}
	Point *get_next(Point *x, Point *a = NULL) {
		if (a != NULL) {
			nextp[x] = a;
			return NULL;
		}
		else if (nextp.find(x) == nextp.end())
			return NULL;
		return nextp[x];
	}
	void generate(Point *a, Point *b, int n) {
		//cerr << "triangle..." << endl;
		int r = n / 3;
		
		int x = (a->x + b->x) / 2;
		int y = a->y + (a->x - b->x) / 4;
		int s = (a->x - b->x) / 4;
	
		Point *p;
		Point *p1 = new_point(x, y + s);
		Point *p2 = new_point(x, y, 0);
		root = p1;
		Point *cur = root;
		int y_upper, y_lower;
		for (int i = 1; i <= r; ++i) {
			x = p1->x;
			y = p1->y + (double) (p2->y - p1->y) / r * i;
			p = new_point(x, y);
			get_next(cur, p);
			x = a->x + (p1->x - a->x) * 0.8 / r * i;
			y_upper = floor(a->y + (p1->y - a->y) * 0.8 / r * i);
			y_lower = ceil (a->y + (p2->y - a->y) * 0.8 / r * i);
			y = floor(y_lower + ((double) rand() / (~0U>>1)) * (y_upper - y_lower));
			get_left(p, new_point(x, y));
			x = b->x + (p1->x - b->x) * 0.8 / r * i;
			y_upper = floor(b->y + (p1->y - b->y) * 0.8 / r * i);
			y_lower = ceil (b->y + (p2->y - b->y) * 0.8 / r * i);
			y = floor(y_lower + ((double) rand() / (~0U>>1)) * (y_upper - y_lower));
			get_right(p, new_point(x, y));
			cur = p;
		}
	}
};
int main(int argc, char **argv) {
	int n, m, k_min, k_max, seed;
	parse_args(argc, argv, n, m, k_min, k_max, seed);
	
	if (k_min != 1 && k_max == 0)
		k_max = min(n - 3, 100);
	
	assert(n <= 100000 && n >= 3);
	assert(m <= 100000);
	assert(k_max <= 100);
	assert(k_min <= k_max || k_max == 0);
	
	g_n = n;
	g_m = m;
	g_seed = seed;	

	init();
	srand(seed);
	
	Point *a, *b, *c;
	Triangle *triangle = new Triangle();
	Ring *ring = new Ring();
	Random_Field *rdf = new Random_Field();
	
	s_idx = (int *) malloc(sizeof(int) * (n+10));
	for (int i = 0; i < n; ++i)
		s_idx[i] = i+1;
	random_shuffle(s_idx, s_idx + n);
	p_idx = s_idx;
	
	int rdf_num, triangle_num, ring_num;
	
	set <Point *> st;

	if (n >= 1000 && k_max == 1) {
		SPCaseOne *spco = new SPCaseOne();
		spco->generate(n);
		cout << n << " " << m << endl;
		for (int i = 1; i <= n; ++i) {
			cout << save_p[i]->x << " " << save_p[i]->y << endl;
			//cout << "[" << save_p[i]->x << "," << save_p[i]->y << "]," << endl;
		}
		for (int i = 1; i <= m; ++i) {
			cout << 1 << " " << spco->p1[rand() % spco->cnt + 1]->idx << endl;
		}
		exit(0);
	}
	else if (n >= 1000 && k_max != 0 && k_max <= 5) {
		a = new_point((int) 1e8, 0);
		b = new_point((int)-1e8, 0);
		SPCaseSmmK *spcm = new SPCaseSmmK();
		triangle->generate(a, b, (k_max+1)*3);
		spcm->generate(a, b, n-(k_max+1)*3-2-1, k_max);
		cout << n << " " << m << endl;
		for (int i = 1; i <= n; ++i) {
			cout << save_p[i]->x << " " << save_p[i]->y << endl;
			//cout << "[" << save_p[i]->x << "," << save_p[i]->y << "]," << endl;
			//cerr << i << endl;
		}
		for (int i = 1; i <= m; ++i) {
			int type = rand() % 2;
			rlst.clear();
			if (type == 0) {
				//cerr << "ring test.." << endl;
				k = k_max == 0 ? rand() % 15 + 5 : k_min + rand() % (k_max - k_min + 1);
				while (k) {
					Point *s;
					while (s = spcm->lst[rand() % spcm->lst.size()], s->removed == true);
					remove(s);
					a = spcm->get_left(s);
					b = spcm->get_right(s);
					if (a != NULL && a->removed == false && st.find(a) == st.end()) st.insert(a), spcm->lst.push_back(a);
					if (b != NULL && b->removed == false && st.find(b) == st.end()) st.insert(b), spcm->lst.push_back(b);
				}
				while (!st.empty())
					spcm->lst.pop_back(), st.erase(st.begin());
			}
			if (type == 1) {
				//cerr << "triangle test..." << endl;
				k = k_max == 0 ? rand() % 15 + 5 : k_min + rand() % (k_max - k_min + 1);
				Point *cur = triangle->root;
				remove(cur);
				cur = triangle->get_next(cur);
				while (k) {
					remove(cur);
					Point *a = triangle->get_left(cur);
					Point *b = triangle->get_right(cur);
					if (k >= 2) {
						int tmp = rand() % 6;
						if (tmp == 0)
							remove(a), remove(b);
						else if (tmp == 1)
							remove(a);
						else if (tmp == 2)
							remove(b);
					}
					else if (k == 1) {
						if (rand() & 1) remove(a);
						else remove(b);
					}
					cur = triangle->get_next(cur);
				}
			}
			int nr = rlst.size();
			cout << nr;
			for (int j = 0; j < rlst.size(); ++j) {
				cout << " " << rlst[j]->idx;
				rlst[j]->removed = false;
			}
			cout << endl;
		}
		exit(0);
	}
	else if (n < 1000) {
		rdf->generate(NULL, NULL, n, SMALL_N);
		rdf->sort_p();
		cout << n << " " << m << endl;
		for (int i = 1; i <= n; ++i) {
			cout << save_p[i]->x << " " << save_p[i]->y << endl;
			//cout << "[" << save_p[i]->x << "," << save_p[i]->y << "]," << endl;
		}
		int t;
		bruteforce(rdf->n, rdf->p, t, q);
		for (int i = 1; i < t; ++i)
			q[i]->layer = 1;
		for (int i = 1; i <= m; ++i) {
			rlst.clear();
			k = k_max == 0 ? min(rand() % rdf->n + 1, rdf->n - 3) : min(rdf->n - 3, k_min + rand() % (k_max - k_min + 1)); 
			for (int i = 1; i <= k; ++i) {
				bruteforce(rdf->n, rdf->p, t, q);
				if (t <= 3) 
					break;
				int s;
				bool ddr = false;
				while (true) {
					s = rand() % (t-1) + 1;
					if (q[s]->fixed == true)
						continue;
					int cnt = 0;
					for (int i = 1; i < t; ++i)
						if (q[i]->layer == 1)
							++cnt; 
					if (cnt == 2 && q[s]->layer == 1)
						continue;
					if (cnt == 1)
						ddr = true;
					break;
				}
				if (ddr)
					break;
				remove(q[s]);
			}
			int nr = rlst.size();
			cout << nr;
			for (int j = 0; j < rlst.size(); ++j) {
				cout << " " << rlst[j]->idx;
				rlst[j]->removed = false;
			}
			cout << endl;
		}
		exit(0);
	}
	else {
		rdf_num = min((int) (n * 0.1), 50);
		triangle_num = min(int ((n - rdf_num) * 0.5), 3000);
		ring_num = n - triangle_num - rdf_num - 10;
		
		a = new_point((int) -1e8, 0);
		b = new_point(0, (int) -1e8);
		c = new_point((int)  1e8, 0);
		
		a->fixed = true;
		b->fixed = true;
		c->fixed = true;
		
		rdf->generate(a, b, rdf_num, LARGE_N);
		ring->generate(b, c, (int) (ring_num * 0.6), ring_num - (int) (ring_num * 0.6));
		triangle->generate(c, a, triangle_num);
		
		rdf->insert(a, b, n - (p_idx - s_idx));
		rdf->sort_p();
		
		cout << n << " " << m << endl;
		for (int i = 1; i <= n; ++i) {
			cout << save_p[i]->x << " " << save_p[i]->y << endl;
		}
	}
	
	
	set <Point *> :: iterator r;
		
	for (int query_id = 1; query_id <= m; ++query_id) {
		rlst.clear();
		if (k_max == 0 && rand() % 5 == 0) {
			//cerr << "MIXED" << endl;
			int t;
			k = min(10, rdf_num - 3);
			for (int i = 1; i <= k; ++i) {
				bruteforce(rdf->n, rdf->p, t, q);
				if (t == 2) 
					break;
				int s;
				while (s = rand() % (t-1) + 1, q[s]->fixed == true);
				remove(q[s]);
			}
			k = rand() % 15 + 5;
			if (rand() & 1) remove(ring->pt1);
			if (rand() & 1) remove(ring->pt2);
			st.clear();
			st.insert(ring->root);
			while (k && !st.empty()) {
					int t = rand() % st.size() + 1;
					r = st.begin();
					for (int i = 1; i < t; ++i) ++r;
					Point *rr = *r;
					a = ring->get_left(rr);
					b = ring->get_right(rr);
					remove(rr);
					st.erase(r);
					if (a != NULL && a->removed == false && st.find(a) == st.end()) st.insert(a);
					if (b != NULL && b->removed == false && st.find(b) == st.end()) st.insert(b);
			}
			k = rand() % 15 + 5;
			Point *cur = triangle->root;
			remove(cur);
			cur = triangle->get_next(cur);
			while (k) {
				remove(cur);
				Point *a = triangle->get_left(cur);
				Point *b = triangle->get_right(cur);
				if (k >= 2) {
					int tmp = rand() % 6;
					if (tmp == 0)
						remove(a), remove(b);
					else if (tmp == 1)
						remove(a);
					else if (tmp == 2)
						remove(b);
				}
				else if (k == 1) {
					if (rand() & 1) remove(a);
					else remove(b);
				}
				cur = triangle->get_next(cur);
			}
		}
		else if (rand() % max(5, (m / 100)) == 0) {
			//cerr << "random test.." << endl;
			int t;
			k = k_max == 0 ? min(rand() % rdf_num + 1, rdf_num - 3) : k_min + rand() % (k_max - k_min + 1);
			for (int i = 1; i <= k; ++i) {
				bruteforce(rdf->n, rdf->p, t, q);
				if (t <= 4) 
					break;
				int s;
				while (s = rand() % (t-1) + 1, q[s]->fixed == true);
				remove(q[s]);
			}
		}
		else if (rand() & 1) {
			//cerr << "ring test.." << endl;
			k = k_max == 0 ? rand() % 15 + 5 : k_min + rand() % (k_max - k_min + 1);
			if ((rand() & 1) && k) remove(ring->pt1);
			if ((rand() & 1) && k) remove(ring->pt2);
			st.clear();
			st.insert(ring->root);
			while (k && !st.empty()) {
					int t = rand() % st.size() + 1;
					r = st.begin();
					for (int i = 1; i < t; ++i) ++r;
					Point *rr = *r;
					a = ring->get_left(rr);
					b = ring->get_right(rr);
					remove(rr);
					st.erase(r);
					if (a != NULL && a->removed == false && st.find(a) == st.end()) st.insert(a);
					if (b != NULL && b->removed == false && st.find(b) == st.end()) st.insert(b);
			}
		}
		else {
			//cerr << "triangle test.." << endl;
			k = rand() % 15 + 5;
			k = k_max == 0 ? rand() % 15 + 5 : k_min + rand() % (k_max - k_min + 1);
			Point *cur = triangle->root;
			remove(cur);
			cur = triangle->get_next(cur);
			while (k) {
				remove(cur);
				Point *a = triangle->get_left(cur);
				Point *b = triangle->get_right(cur);
				if (k >= 2) {
					int tmp = rand() % 6;
					if (tmp == 0)
						remove(a), remove(b);
					else if (tmp == 1)
						remove(a);
					else if (tmp == 2)
						remove(b);
				}
				else if (k == 1) {
					if (rand() & 1) remove(a);
					else remove(b);
				}
				cur = triangle->get_next(cur);
			}
		}
		int nr = rlst.size();
		cout << nr;
		for (int j = 0; j < rlst.size(); ++j) {
			cout << " " << rlst[j]->idx;
			rlst[j]->removed = false;
		}
		cout << endl;
	}
}
