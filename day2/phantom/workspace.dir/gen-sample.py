import os, sys
from tqdm import tqdm

if __name__ == '__main__':
	
	os.system('g++ -o bruteforce bruteforce.cpp -O2')
	os.system('g++ -o maker maker.cpp -O2')
	os.system('g++ -o phantom phantom.cpp -O2')
	os.system('g++ -o phantom-encode phantom-encode.cpp -O2')
	
	if os.path.exists(sys.argv[1]) == False:
		os.makedirs(sys.argv[1])
	
	s = [[1000, 100000, 1],
		 [1000, 100000, 2],
		 [100000, 100000, 0]]

	seeds = [1136593790, 1789104461, 1611912694]
	
	for i in tqdm(range(3)):
		n, m, k = s[i]
		seed = seeds[i]
		output = 'sample-phantom{}-decoded.in'.format(i+2)
		if k == 0:
			os.system('./maker {} {} -seed={} >{}'.format(n, m, seed, os.path.join(sys.argv[1], output)))
		else:
			os.system('./maker {} {} -k_max={} -seed={} >{}'.format(n, m, k, seed, os.path.join(sys.argv[1], output)))
		
		output2 = 'sample-phantom{}.in'.format(i+2)	
		output3 = 'sample-phantom{}.ans'.format(i+2)	
		os.system('./phantom-encode <{} >{}'.format(os.path.join(sys.argv[1], output), os.path.join(sys.argv[1], output2)))
		os.system('./phantom <{} >{}'.format(os.path.join(sys.argv[1], output2), os.path.join(sys.argv[1], output3)))
		os.system('rm -r {}'.format(os.path.join(sys.argv[1], output)))
		
	os.system('rm -r bruteforce maker phantom phantom-encode')

