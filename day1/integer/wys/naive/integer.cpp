// integer naive.cpp by wys @2017-06-24

#include <stdio.h>
#include <stdint.h>

const int MAXN = 1000005;
const int MAXM = MAXN * 30;
const int MAXLEN = MAXM / 32;

uint32_t A[MAXLEN];

void _add1(int p) {
	while (A[p] == ~0u) {
		A[p++] = 0;
	}
	++A[p];
}

void _sub1(int p) {
	while (A[p] == 0) {
		A[p++] = ~0u;
	}
	--A[p];
}

void _add(int pos, uint32_t val) {
	if ((A[pos] += val) < val) {
		_add1(pos + 1);
	}
}

void _sub(int pos, uint32_t val) {
	if (A[pos] < val) {
		_sub1(pos + 1);
	}
	A[pos] -= val;
}

void add(int _a, int b) {
	if (_a == 0) return;
	
	bool neg = _a < 0;
	uint32_t a = neg ? -_a : _a;
	
	// b/32, b%32, 32-b%32, b/32+1, 0, b%32
	
	int pos = b >> 5;
	int off = b & 31;
	int len1 = 32 - off;
	
	// log(1e9) = 30
	uint32_t part1 = len1 >= 30 ? a : (a & ((1u << len1) - 1));
	uint32_t part2 = len1 >= 30 ? 0 : (a >> len1);
	
	if (part1) (neg ? _sub : _add)(pos, part1 << off);
	if (part2) (neg ? _sub : _add)(pos + 1, part2);
}

int query(int k) {
	return ((A[k >> 5]) >> (k & 31)) & 1u;
}

int main() {
	
	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	
	for (int i = 0; i < n; i++) {
		int op, a, b, k;
		scanf("%d", &op);
		if (op == 1) {
			scanf("%d%d", &a, &b);
			add(a, b);
		} else {
			scanf("%d", &k);
			putchar("01"[query(k)]);
			putchar('\n');
		}
	}
}
