for ((i=1;i<=25;i++)) do (
	echo -n "$i : "
	(sleep 3 && killall $1.exe > /dev/null 2> /dev/null) & 
	echo "" | time ./$1.exe < ../data/$i.in > tmp.out ;
	killall -9 sleep > /dev/null 2> /dev/null;
	diff ../data/$i.ans tmp.out 2>&1 > dif ; wc -l dif ; rm dif ; rm tmp.out
) done ;
