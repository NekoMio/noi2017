# integer gen.py by wys @2017-06-24

from random import randint

n, t1, t2, t3 = map(int, raw_input().split(" "))

if (t1 == 1) or (t1 == 2):
	L = 1
	R = 1
else:
	L = 1
	R = 10 ** 9

if t2 == 1:
	m = 30
elif t2 == 2:
	m = 100
elif t2 == 3:
	m = n
elif t2 == 4:
	m = 30 * n

print n, t1, t2, t3

x = 0

cnt = randint(n * 0.4, n * 0.6)
for i in xrange(n):
	if t3 == 1:
		op = 1 if i < cnt else 2
	else:
		op = randint(1, 2)
	
	if op == 1:
		while 1:
			a = randint(L, R)
			if t1 != 1:
				a *= randint(0, 1) * 2 - 1
			b = randint(0, m)
			if x + (a << b) >= 0:
				x += a << b
				print 1, a, b
				break
	else:
		k = randint(0, m)
		print 2, k
