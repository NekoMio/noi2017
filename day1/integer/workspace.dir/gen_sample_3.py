from random import randint

n = 200000
print "%d 3 4 2" % n

m = n * 30

last = 1

for i in range(n):
	if i == 0:
		print "1 1 %d" % m
	else:
		if randint(1, 2) == 1:
			if last == 1:
				last = 0
				print "1 -1 0"
			else:
				last = 1
				print "1 1 0"
		else:
			print "2 %d" % randint(0, m)
