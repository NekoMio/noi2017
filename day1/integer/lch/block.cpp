#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int bitnum = 60;
const int blocknum = 8;
const int maxn = 1000000;
const int Block = 1 << blocknum;
const int maxB = (30 * maxn + 50) / (bitnum * Block) + 1;
const int maxN = maxB * Block;

const long long ALL = (1LL << bitnum) - 1LL;

int N, M;
int X[maxB];
long long x[maxN];

inline int hash(long long x)
{
	if (x == 0) return 1;
	if (x == ALL) return 2;
	return 3;
}

inline void down(int s)
{
	int m = s >> blocknum;
	if (X[m] == 3) return;
	int bound = (m + 1) << blocknum;
	for (int i = m << blocknum; i < bound; ++i)
		x[i] = (X[m] == 1)? 0 : ALL;
	X[m] = 3;
}

void up_pos(int s, long long u)
{
	while (u)
	{
		int m = s >> blocknum;
		if (s == (m << blocknum) && X[m] == 2 && u == 1)
		{
			X[m] = 1; s += Block; 
			continue;
		}
		down(s);
		int flag = 0, ss = s;
		int bound = min(((m + 1) << blocknum) - 1, N - 1);
		for (x[s] += u; s < bound && (x[s] >> bitnum) != 0LL; flag |= hash(x[s++]))
		{
			x[s+1] += x[s] >> bitnum;
			x[s] &= ALL;
		}
		if (s == bound)
		{
			u = x[bound] >> bitnum;
			x[bound] &= ALL;
			for (int i = m << blocknum; i < ss && flag != 3; ++i)
				flag |= hash(x[i]);
			X[m] = flag | hash(x[bound]);
			s = bound + 1;
		}
		else
			break;
	}
}

void up_neg(int s, long long u)
{
	while (u)
	{
		int m = s >> blocknum;
		if (s == (m << blocknum) && X[m] == 1 && u == 1)
		{
			X[m] = 2; s += Block; 
			continue;
		}
		down(s);
		int flag = 0, ss = s;
		int bound = min(((m + 1) << blocknum) - 1, N - 1);
		for (x[s] -= u; s < bound && x[s] < 0; flag |= hash(x[s++]))
		{
			x[s] += 1LL << bitnum;
			--x[s+1];
		}
		if (s == bound)
		{
			u = x[bound] < 0;
			x[bound] += u << bitnum;
			for (int i = m << blocknum; i < ss && flag != 3; ++i)
				flag |= hash(x[i]);
			X[m] = flag | hash(x[bound]);
			s = bound + 1;
		}
		else
			break;
	}
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	if (t2 == 1) N = 30; else
	if (t2 == 2) N = 100; else
	if (t2 == 3) N = n; else
		N = 30 * n;
	M = (N + 50) / (bitnum * Block) + 1;
	N = M * Block;

	for (int i = 0; i < M; ++i) X[i] = 1;
	for (int t = 0; t < n; ++t)
	{
		int sign;
		scanf("%d", &sign);
		if (sign == 1)
		{
			int ta, b, idx;
			long long a, tmp;
			scanf("%d%d", &ta, &b);
			a = (long long)ta;
			idx = b / bitnum;
			b = b % bitnum;
			if (a > 0)
			{
				down(idx);
				x[idx] += (a & ((1LL << (bitnum - b)) - 1LL)) << b;
				tmp = (a >> (bitnum - b)) + (x[idx] >> bitnum);
				x[idx] &= ALL;
				up_pos(idx + 1, tmp);
			}
			else if (a < 0)
			{
				a = -a;
				down(idx);
				x[idx] -= (a & ((1LL << (bitnum - b)) - 1LL)) << b;
				tmp = a >> (bitnum-b);
				if (x[idx] < 0LL)
				{
					x[idx] += 1LL << bitnum;
					++tmp;
				}
				up_neg(idx + 1, tmp);
			}
		}
		else
		{
			int k, idx, m;
			scanf("%d", &k);
			idx = k / bitnum;
			m = idx >> blocknum;
			if (X[m] == 3)
			{
				k = k % bitnum;
				if (x[idx] & (1LL << k))
					printf("1\n");
				else
					printf("0\n");
			}
			else
				printf("%d\n", (X[m] == 1)? 0 : 1);
		}
	}
	return 0;
}