#include <cstdio>
#include <bitset>
#include <assert.h>
#include <iostream>
using namespace std;

const int maxn = 1000000;
const int maxN = 30 * maxn + 1 + 50;

int N;
bitset<maxN> x;

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	if (t2 == 1) N = 30 + 1; else
	if (t2 == 2) N = 100 + 1; else
	if (t2 == 3) N = n + 1; else
		N = 30 * n + 1;
	N += 50;
	for (int t = 0; t < n; ++t)
	{
		int sign;
		scanf("%d", &sign);
		if (sign == 1)
		{
			int tmp, a, b;
			scanf("%d%d", &a, &b);
			if (a > 0)
			{
				for (int i = b, u = 0; (a || u) && i < N; ++i, a >>= 1)
				{
					tmp = x[i] + (a&1) + u;
					x[i] = tmp & 1;
					u = tmp >> 1;
				}
			}
			else if (a < 0)
			{
				a = -a;
				int u = 0;
				for (int i = b; (a || u != 0) && i < N; ++i, a >>= 1)
				{
					tmp = x[i] - (a&1) + u;
					if (tmp < 0)
					{
						tmp += 2;
						u = -1;
					}
					else
						u = 0;
					x[i] = tmp;
				}
				if (u == -1)
				{
					cerr << "Error: x < 0\t";
					return 1;
				}
			}
		}
		else
		{
			int k;
			scanf("%d", &k);
			if (x[k])
				printf("1\n");
			else
				printf("0\n");
		}
	}

	return 0;
}