#include <cstdio>
#include <fstream>
#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
using namespace std;

char cs[256];
FILE *fin, *fout;
int A[256], N, t1, t2, t3;

int myGetLine(FILE *file = fin)
{
	if (fgets(cs, 256, file) == NULL) return -1;
	int len = strlen(cs), ret = 0, j = 0;
	for (int i = 0; i < len; ++i)
		if (cs[i] == ' ' || cs[i] == '\n')
		{
			if (i == 0 || cs[i-1] == '\0')
				return -1;
			cs[i] = '\0';
			A[ret++] = atoi(cs+j);
			j = i + 1;
		}
		else
		{
			assert('0' <= cs[i] && cs[i] <= '9' || cs[i] == '-');
			if (cs[i] == '-')
				assert(i == 0 || cs[i-1] == '\0');
		}
	return ret;
}

int main(int argc, char **argv)
{
	ifstream cin("data_range.in");
	for (int i = 1; i <= 29; ++i)
	{
		cerr << "the " << i << "-th data/sample" << endl;
		int sN, st1, st2, st3;
		cin >> sN >> st1 >> st2 >> st3;
		if (i <= 25)
		{
			sprintf(cs, "../data/%d.in", i);
			fin = fopen(cs, "r");
			sprintf(cs, "../data/%d.ans", i);
			fout = fopen(cs, "r");
		}
		else
		{
			sprintf(cs, "../down/%d.in", i-25);
			fin = fopen(cs, "r");
			sprintf(cs, "../down/%d.ans", i-25);
			fout = fopen(cs, "r");	
		}
		assert(myGetLine() == 4);
		N = A[0]; t1 = A[1]; t2 = A[2]; t3 = A[3];
		assert(N <= sN && t1 == st1 && t2 == st2 && t3 == st3);
		bool query = false;
		for (int i = 0; i < N; ++i)
		{
			int m = myGetLine();
			int sign = A[0];
			assert(sign == 1 || sign == 2);
			if (sign == 1)
			{
				assert(m == 3);
				int a = A[1], b = A[2];
				if (t1 == 1) 
					assert(a == 1);
				else if (t1 == 2)
					assert(a == -1 || a == 0 || a == 1);
				else
					assert(-1000000000 <= a && a <= 1000000000);
				if (t2 == 1)
					assert(0 <= b && b <= 30);
				else if (t2 == 2)
					assert(0 <= b && b <= 100);
				else if (t2 == 3)
					assert(0 <= b && b <= N);
				else
					assert(0 <= b && b <= 30*N);
				if (t3 == 1)
					assert(query == false);
			}
			else
			{
				assert(m == 2);
				int k = A[1];
				if (t2 == 1)
					assert(0 <= k && k <= 30);
				else if (t2 == 2)
					assert(0 <= k && k <= 100);
				else if (t2 == 3)
					assert(0 <= k && k <= N);
				else
					assert(0 <= k && k <= 30*N);
				query = true;
				assert(myGetLine(fout) == 1);
				assert(A[0] == 0 || A[0] == 1);
			}
		}
		fclose(fin);
		fclose(fout);
	}
	cerr << "OK" << endl;

	return 0;
}