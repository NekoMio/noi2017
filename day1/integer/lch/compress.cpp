#include <cstdio>
#include <bitset>
#include <cstring>
#include <assert.h>
#include <iostream>
using namespace std;

const int maxn = 1000000;
const int bitnum = 60;
const int maxN = (30 * maxn + 50) / bitnum + 1;

int N;
long long x[maxN];

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	if (t2 == 1) N = 30; else
	if (t2 == 2) N = 100; else
	if (t2 == 3) N = n; else
		N = 30 * n;
	N = (N + 50) / bitnum + 1;

	memset(x, 0, sizeof(x));
	for (int t = 0; t < n; ++t)
	{
		int sign;
		scanf("%d", &sign);
		if (sign == 1)
		{
			int ta, b, idx;
			long long a, tmp1, tmp2;
			scanf("%d%d", &ta, &b);
			a = (long long)ta;
			idx = b / bitnum;
			b = b % bitnum;
			if (a > 0)
			{
				tmp1 = a & ((1LL << (bitnum - b)) - 1LL);
				tmp2 = a >> (bitnum-b);
				x[idx] += tmp1 << b;
				x[idx+1] += tmp2 + (x[idx] >> bitnum);
				x[idx] &= (1LL << bitnum) - 1LL;
				for (int i = idx+1; i < N-1 && ((x[i] >> bitnum) != 0LL); ++i)
				{
					x[i+1] += x[i] >> bitnum;
					x[i] &= (1LL << bitnum) - 1LL;
				}
				assert(x[N-1] < (1LL << bitnum));
			}
			else if (a < 0)
			{
				a = -a;
				tmp1 = (a & ((1LL << (bitnum - b)) - 1LL));
				tmp2 = a >> (bitnum-b);
				x[idx] -= tmp1 << b;
				x[idx+1] -= tmp2;
				if (x[idx] < 0LL)
				{
					x[idx] += 1LL << bitnum;
					--x[idx+1];
				}
				for (int i = idx+1; i < N-1 && (x[i] < 0); ++i)
				{
					x[i] += 1LL << bitnum;
					--x[i+1];
				}
				if (x[N-1] < 0)
				{
					cerr << "Error: x < 0\t";
					return 1;
				}
			}
/*
			for (int i = 0; i < N; ++i)
				if (x[i] != 0) cerr << i << ' ' << x[i] << endl;
			cerr << endl;
*/
		}
		else
		{
			int k, idx;
			scanf("%d", &k);
			idx = k / bitnum;
			k = k % bitnum;
			if (x[idx] & (1LL << k))
				printf("1\n");
			else
				printf("0\n");
		}
	}

	return 0;
}