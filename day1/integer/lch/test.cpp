#include <cstdio>
#include <cstdlib>
#include <string>
#include <assert.h>
#include <sstream>
#include <iostream>
using namespace std;

int run(string cmd)
{
	return system((char*)cmd.c_str());
}

string ItoS(int x)
{
	string y;
	stringstream f;
	f << x; f >> y;
	return y;
}

int main(int argc, char **argv)
{
	int s_data = atoi(argv[1]);
	int t_data = atoi(argv[2]);
	assert(1 <= s_data && s_data <= t_data && t_data <= 29);
	string code = "./block";
	string out = "./test.out";
	if (argc > 3)
		out = (string)argv[3];

	int cnt = 0;
	for (int i = s_data; i <= t_data; ++i)
	{
		string in, ans;
		if (i <= 25)
		{
			in = "../data/" + ItoS(i) + ".in";
			ans = "../data/" + ItoS(i) + ".ans";
		}
		else
		{
			in = "../down/" + ItoS(i-25) + ".in";
			ans = "../down/" + ItoS(i-25) + ".ans";
		}
		cerr << "testing the " << i << "-th data" << endl;
		cerr << "..."; 
		if (run("time " + code + " " + in + " " + out))
			cerr << "RE" << endl;
		else
		{
			if (run("./judge " + out + " " + ans))
				cerr << "WA" << endl;
			else
			{
				cerr << "AC" << endl;
				if (1 <= i && i <= 25) ++cnt;
			}
		}
	}
	cerr << "score = " << 4 * cnt << endl;

	return 0;
}