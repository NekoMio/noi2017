#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
const int mo=998244353;
int quick(int k1,int k2){
	int k3=1;
	while (k2){
		if (k2&1) k3=1ll*k3*k1%mo;
		k2>>=1; k1=1ll*k1*k1%mo;
	}
	return k3;
}
int f[1100][2100],n,K,p,pre[2100],fir[1100],A[1100],B[1100],len,C[1100];
long long D[2100];
int getp(){
	int k1,k2; scanf("%d%d",&k1,&k2); return 1ll*k1*quick(k2,mo-2)%mo;
}
void mul(int *x,int *y,int *ans,int flag=0){
	memset(D,0x00,sizeof D);
	for (int i=0;i<len;i++)
		for (int j=0;j<len;j++) 
			if (D[i+j]<0) D[i+j]+=1ll*x[i]*y[j];
			else D[i+j]+=1ll*(x[i]-mo)*y[j];
	for (int i=len+len-2;i>=len;i--){
		D[i]=(D[i]%mo+mo)%mo; int w=D[i]; 
		for (int j=0;j<=len;j++) 
			if (D[i-len+j]>0) D[i-len+j]-=1ll*A[j]*w;
			else D[i-len+j]-=1ll*(A[j]-mo)*w;
	}
	for (int i=0;i<len;i++) ans[i]=(D[i]%mo+mo)%mo;
}
void getB(int n){
	memset(B,0x00,sizeof B);
	memset(C,0x00,sizeof C);
	C[1]=1; B[0]=1;
	while (n){
		if (n&1) mul(C,B,B,1);
		n>>=1; mul(C,C,C);
	}
}
int get(int K){
	if (K==0) return quick(1-p,n);
	if (n==1){
		int ans=0;
		for (int i=0;i<=K;i++) ans=(ans+1ll*quick(p,i)*(1-p))%mo;
		return ans;
	}
	memset(f,0x00,sizeof f);
	memset(pre,0x00,sizeof pre);
	pre[0]=1;
	for (int i=K;i>=0;i--){
		int lim=0,w=1ll*quick(p,i)*(1-p)%mo; w=(w+mo)%mo;
		if (i) lim=K/i; else lim=2*K+1;
		for (int j=1;j<=lim;j++)
			for (int k=0;k<j;k++)
				f[i][j]=(f[i][j]+1ll*pre[k]*w%mo*(pre[j-k-1]+f[i][j-k-1]))%mo;
		if (i) for (int j=1;j<=lim;j++) pre[j]=(pre[j]+f[i][j])%mo;
	}
	if (n<=2*K+1) return (f[0][n]+pre[n])%mo;
	len=K+1; A[K+1]=1; //cout<<f[0][n]<<endl;
	for (int i=0;i<=K;i++) A[K-i]=1ll*(p-1)*pre[i]%mo;
	getB(n-1-K); int ans=0;
	for (int i=0;i<len;i++) ans=(ans+1ll*B[i]*f[0][i+1+K])%mo;
//	for (int i=0;i<len;i++) cout<<(A[i]+B[i])%mo<<" "; cout<<endl;
//	cout<<(ans+mo)%mo<<endl;
	return ans;
}
int main(){
	//freopen("pool.in","r",stdin);
	//freopen("pool.out","w",stdout);
	scanf("%d%d",&n,&K); p=getp();
	int ans=(get(K)-get(K-1))%mo; ans=(ans+mo)%mo;
	printf("%d\n",ans);
	return 0;
}
