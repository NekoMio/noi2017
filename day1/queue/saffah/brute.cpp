#include <bits/stdc++.h>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)
using namespace std;

const int MAXN = 200000 + 7;
const int MAXK = 50 + 3;
const int MAXSL = 1e7 + 9;
const int MOD = 998244353;

int n;
char a[MAXN];
int b[MAXN];
char c[MAXN][MAXK];
char qs[MAXSL];

void build()
{
	f(i, 1, n)
		for(int s = i, j = 0; j < MAXK; ++j, s = b[s])
			if(s)
				c[i][j] = a[s];
			else
			{
				c[i][j] = 0;
				break;
			}
}

int subq(char *a, int k)
{
	int ans = 0;
	f(i, 1, n)
		ans += !strncmp(a, c[i], k);
	return ans;
}

int main()
{
	int m; scanf("%d%d", &n, &m);
	f(i, 1, n)
	{
		int x; scanf("%d", &x);
		a[i] = x + '0';
	}
	build();
	while(m--)
	{
		int op; scanf("%d", &op);
		if(op == 1)
		{
			int i, j; scanf("%d%d", &i, &j);
			b[i] = j;
			build();
		}
		else if(op == 2)
		{
			int i; scanf("%d", &i);
			b[i] = 0;
			build();
		}
		else
		{
			int k;
			scanf("%s%d", qs, &k);
			int ql = strlen(qs);
			int ans = 1;
			f(i, 0, ql - k)
				if(!(ans = (long long) ans * subq(qs + i, k) % MOD))
					break;
			printf("%d\n", ans);
		}
	}
	return 0;
}
