#include<assert.h>
#include<cstdio>
#include<cstring>
#define MAXN 200000
#define MAXM 500000
#define MAXK 50
#define MAXSTR 10000000
int n,m,one=1,tot_len,c,maxk;
char s[MAXSTR+10];
int nex[MAXN+10],bef[MAXN+10];
int read_int(int min_lim,int max_lim)
{
	int x;
	scanf("%d",&x);
	assert(min_lim<=x);
	assert(x<=max_lim);
	return x;
}
void read_string(int min_lim,int max_lim)
{
	scanf("%s",s);
	int len=strlen(s),i;
	assert(min_lim<=len);
	assert(len<=max_lim);
	tot_len+=len;
	for(i=0;i<len;++i)
	{
		assert('0'<s[i]);
		assert(s[i]<='6');
		if(s[i]!='1')
			one=0;
	}
}
int main()
{
	int ord,a,b,aa;
	n=read_int(1,MAXN);
	m=read_int(1,MAXM);
	for(int i=1;i<=n;++i)
		if(read_int(1,6)!=1)
			one=0;
	for(int i=1;i<=m;++i)
	{
		ord=read_int(1,3);
		if(ord==1)
		{
			a=read_int(1,n);
			b=read_int(1,n);
			assert(nex[a]==0);
			assert(bef[b]==0);
			for(aa=a;nex[aa];aa=nex[aa]);
			assert(aa!=b);
			nex[a]=b;
			bef[b]=a;
		}
		if(ord==2)
		{
			a=read_int(1,n);
			assert(nex[a]!=0);
			bef[nex[a]]=0;
			nex[a]=0;
			++c;
		}
		if(ord==3)
		{
			read_string(1,MAXSTR);
			a=read_int(1,strlen(s));
			if(maxk<a)
				maxk=a;
		}
	}
	printf("%d\t%d\t%d\t%d\t%d\t%d\n",n,m,maxk,tot_len,c,one);
	return 0;
}
