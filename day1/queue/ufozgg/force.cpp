#include<cstdio>
#include<cstdlib>
#include<cstring>
#define MAXN 200100
#define MAXM 500100
#define MAXDEL 1010
#define MAXK 55
#define MAXS 10001000
#define MOD 998244353
int n,m,k;
char ch[MAXN];
char s[MAXS];
char tmp[10];
int nex[MAXN],bef[MAXN];
void link_0(int a,int b)
{
	while(nex[a])
		a=nex[a];
	while(bef[b])
		b=bef[b];
	nex[a]=b;
	bef[b]=a;
}
void cut_0(int a)
{
	bef[nex[a]]=0;
	nex[a]=0;
}
int ask_0()
{
	long long ret=1;
	int i,j,len,now,cnt,w;
	len=strlen(s);
	for(i=0;i+k<=len;++i)
	{
		cnt=0;
		for(w=1;w<=n;++w)
		{
			now=w;
			for(j=0;j<k;++j)
			{ 
				if(ch[now]!=s[i+j])
					break;
				now=nex[now];
			}
			if(j==k)
				++cnt;
		}
		ret=ret*cnt%MOD;
		if(ret==0)
			return ret;
	}
	return ret;
}
int main()
{
	int i,a,b;
	int ord;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i)
	{
		scanf("%s",tmp);
		ch[i]=tmp[0];
	}
	for(i=1;i<=m;++i)
	{
		scanf("%d",&ord);
		if(ord==1)
		{
			scanf("%d%d",&a,&b);
			link_0(a,b);
		}
		if(ord==2)
		{
			scanf("%d",&a);
			cut_0(a);
		}
		if(ord==3)
		{
			scanf("%s%d",s,&k);
			printf("%d\n",ask_0());
		}
	}
	return 0;
}
