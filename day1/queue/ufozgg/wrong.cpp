#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#define MAXN 200100
#define MAXM 500100
#define MAXDEL 1010
#define MAXK 55
#define MAXS 10001000
#define MOD 998244353
#define P0 7
#define MOD0 2000003
#define P1 11
#define MOD1 179999993
int n,m,k;
char ch[MAXN];
char s[MAXS];
char tmp[10];
int nex[MAXN],bef[MAXN];
int p02[MAXK];
int p12[MAXK];
int last[MAXK][MOD0];
int top;
int next[MAXK*MAXN+MAXDEL*MAXK*MAXK],key[MAXK*MAXN+MAXDEL*MAXK*MAXK],v[MAXK*MAXN+MAXDEL*MAXK*MAXK];
void add1(int pp0,int pp1,int len,int k0)
{
	for(int i=last[len][pp0];i;i=next[i])
		if(key[i]==pp1)
		{
			v[i]+=k0;
			return;
		}
	++top;
	next[top]=last[len][pp0];
	last[len][pp0]=top;
	key[top]=pp1;
	v[top]=k0;
}
int askk(int pp0,int pp1)
{
	int ret=0,i;
	for(i=last[k][pp0];i;i=next[i])
		if(key[i]==pp1)
			ret+=v[i];
	return ret;
}
void solve_1(int a,int b,int k0)
{
	int i,j,h,t,used;
	int pp0;
	int pp1;
	for(i=2;i<=50;++i)//length
	{
		h=a;
		t=b;
		used=2;
		while(used<i&&bef[h])
		{
			++used;
			h=bef[h];
		}
		while(used<i&&nex[t])
		{
			++used;
			t=nex[t];
		}
		if(used!=i)
			break;
		pp0=0;
		pp1=0;
		for(j=h;j!=nex[t];j=nex[j])
		{
			pp0=(pp0*P0+ch[j]-'0')%MOD0;
			pp1=(pp1*P1+ch[j]-'0')%MOD1;
		}
		while(h!=b&&t)
		{
			add1(pp0,pp1,i,k0);
			pp0=(pp0-(ch[h]-'0')*p02[i-1]%MOD0+MOD0)%MOD0;
			pp1=(pp1-(ch[h]-'0')*p12[i-1]%MOD1+MOD1)%MOD1;
			h=nex[h];
			t=nex[t];
			pp0=(pp0*P0+ch[t]-'0')%MOD0;
			pp1=(pp1*P1+ch[t]-'0')%MOD1;
		}
	}
}
void link_1(int a,int b)
{
	nex[a]=b;
	bef[b]=a;
	solve_1(a,b,1);
}
void cut_1(int a)
{
	int b=nex[a],h,t;
	solve_1(a,b,-1);
	bef[b]=nex[a]=0;
}
int ask_1()
{
	int ret=1;
	int i,len,h,t;
	int pp0=0;
	int pp1=0;
	len=strlen(s);
	for(i=0;i<k;++i)
	{
		pp0=(pp0*P0+s[i]-'0')%MOD0;
		pp1=(pp1*P1+s[i]-'0')%MOD1;
	}
	h=0;
	t=k-1;
	while(t<len)
	{
		ret=ret*askk(pp0,pp1)%MOD;
		if(ret==0)
			return 0;
		pp0=(pp0-(s[h]-'0')*p02[k-1]%MOD0+MOD0)%MOD0;
		pp1=(pp1-(s[h]-'0')*p12[k-1]%MOD1+MOD1)%MOD1;
		++h;
		++t;
		pp0=(pp0*P0+s[t]-'0')%MOD0;
		pp1=(pp1*P1+s[t]-'0')%MOD1;
	}
	return ret;
}
int main()
{
	int i,a,b;
	int ord;
	p02[0]=1;
	p12[0]=1;
	for(i=1;i<=50;++i)
	{
		p02[i]=p02[i-1]*P0%MOD0;
		p12[i]=p12[i-1]*P1%MOD1;
	}
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i)
	{
		scanf("%s",tmp);
		ch[i]=tmp[0];
		add1(ch[i]-'0',ch[i]-'0',1,1);
	}
	for(i=1;i<=m;++i)
	{
		scanf("%d",&ord);
		if(ord==1)
		{
			scanf("%d%d",&a,&b);
			link_1(a,b);
		}
		if(ord==2)
		{
			scanf("%d",&a);
			cut_1(a);
		}
		if(ord==3)
		{
			scanf("%s%d",s,&k);
			printf("%d\n",ask_1());
		}
	}
	return 0;
}
