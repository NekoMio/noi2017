ret = [["测试点编号","$n$","$m$","$k$","$\\sum |s|$","$c$","全为\\texttt{1}"]]
for datum in prob['data']:
    args = datum['args']
    row = [
        ','.join(map(str, datum['cases'])),
        "$\\leq %s$" % tools.hn(args['maxn']) if args['maxn'] > 1 else "$=1$",
        "$\\leq %s$" % tools.hn(args['maxm']),
        "$\\leq %s$" % tools.hn(args['maxk']) if args['maxk'] > 1 else "$=1$",
        "$\\leq %s$" % tools.hn(args['maxslen']),
        "$\\leq %s$" % tools.hn(args['maxdel']) if args['maxdel'] > 0 else "$=0$",
		"Yes" if args['all1'] > 0 else "No"
    ]
    ret.append(row)
common.log.debug(u'输出调试信息')
return merge_ver(ret)
