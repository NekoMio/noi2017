#include <bits/stdc++.h>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)
using namespace std;
void ensureImpl(bool x, int line, const char *info)
{
	if(!x)
	{
		printf("failed on line %d: %s\n", line, info);
	}
}
#define ensure(x) ensureImpl(x, __LINE__, #x)

const int MAXN = 200000;
const int MAXM = 500000;
const int MAXK = 50;
const int MAXSL = 10000000;
const int MOD = 998244353;
const int MAXDEL = 1000;
const int MAXA = 6;

int R()
{
	int a = rand() & 32767;
	int b = rand() & 32767;
	return (a << 10) ^ (b >> 2);
}
const int RMAX = (32768 << 10) - 1;

namespace STD
{
	const int MAXN = ::MAXN + 7;
	const int MAXK = ::MAXK + 3;
	const int MAXSL = ::MAXSL + 7;
	const int MAXDEL = ::MAXDEL + 7;

	const int MAXNODE = MAXN * MAXK + MAXDEL * MAXK * MAXK / 2 + 7;
	
	struct Link
	{
		int ls, rs, c;
	} l[MAXN];

	struct Node
	{
		int son[6], cnt, go;
	} e[MAXNODE];
	int ntop;

	int newNode(int fa, int x)
	{
		ensure(ntop < MAXNODE);
		int g = ntop++;
		if(fa)
		{
			int &c = e[e[fa].go].son[x];
			if(!c)
				c = newNode(e[fa].go, x);
			e[g].go = c;
		}
		return g;
	}

	void insertSingle(int c)
	{
		int &s = e[0].son[c];
		if(!s)
			s = newNode(0, c);
		++e[s].cnt;
	}

	void opr(int x, int y)
	{
		bool join;
		if(y)
		{
			join = true;
			ensure(!l[x].rs);
			ensure(!l[y].ls);
			// ensure(x and y not together);
			l[x].rs = y;
			l[y].ls = x;
		}
		else
		{
			join = false;
			y = l[x].rs;
			ensure(y);
		}
		
		int lef = x;
		for(int i = 2; l[lef].ls && i < MAXK; ++i)
			lef = l[lef].ls;
		int c = e[0].son[l[lef].c], lxc = 0, lxi = 0;
		ensure(c);
		
		for(;;)
		{
			int i, t;
			if(lxc)
			{
				i = lxi--;
				t = y;
				c = lxc = e[lxc].go;
				ensure(c);
			}
			else
			{
				i = 1;
				t = l[lef].rs;
				if(lef == x)
					lxc = c;
			}
			for(; t && i < MAXK; ++i)
			{
				// printf("i %d, t %d, c %d\n", i, t, c);
				if(!e[c].son[l[t].c])
				{
					if(!lxc || !join)
						ensure(0);
					e[c].son[l[t].c] = newNode(c, l[t].c);
				}
				c = e[c].son[l[t].c];
				if(lxc)
				{
					// printf("+CNT %d\n", c);
					if(join)
						++e[c].cnt;
					else
						--e[c].cnt;
				}
				if(t == x)
				{
					lxc = c;
					lxi = i;
				}
				t = l[t].rs;
			}
			if(lef == x)
				break;
			else
				lef = l[lef].rs;
		}
		
		if(!join)
		{
			l[x].rs = 0;
			l[y].ls = 0;
		}
	}

	char buf[MAXSL];
	int nn;
	
	void init(const vector<int> &a)
	{
		memset(l, 0, sizeof(l));
		memset(e, 0, sizeof(e));
		ntop = 1;
		int n = a.size() - 1;
		nn = n;
		f(i, 1, n)
		{
			l[i].c = a[i] - 1;
			insertSingle(l[i].c);
		}
	}
	
	void link(int x, int y)
	{
		opr(x, y);
	}
	void cut(int x)
	{
		opr(x, 0);
	}
	string suggest(int k, int len)
	{
		int d = R() % nn + 1;
		string ans = string("") + (char) ('1' + l[d].c);
		--len;
		int ok = k;
		int c = 0;
		while(--k)
		{
			d = l[d].rs;
			if(!d)
				return "";
			c = e[c].son[l[d].c];
			ensure(c);
			ans += '1' + l[d].c;
			--len;
		}
		// if(ok >= 40)
			// printf("wow!\n");
		while(len--)
		{
			int T = 20, t = 0, s = -1;
			while(T--)
			{
				s = R() % MAXA;
				t = e[c].son[s];
				if(t && e[t].cnt)
					break;
			}
			if(!t || !e[t].cnt)
				return ans;
			c = e[t].go;
			ans += '1' + s;
			ensure(c || ok == 1);
		}
		return ans;
	}

}

struct Opr
{
	int op, i, j;
	string s;
};

Opr link(int i, int j)
{
	return (Opr) {1, i, j, "???"};
}
Opr cut(int i)
{
	return (Opr) {2, i, -1, "???"};
}
Opr query(string s, int k)
{
	return (Opr) {3, k, -1, s};
}

struct Node
{
	Node *ls, *rs, *lm, *rm;
	// rm valid iff no ls
	Node(){ls = rs = 0; lm = rm = this;}
};

struct File
{
	vector<int> a;
	vector<Opr> b;
	vector<Node> ns;
	string no;
	File(int n): no(to_string(n))
	{
		printf("hello %d at %f s\n", n, (double) clock() / CLOCKS_PER_SEC);
		srand(n);
	}
	~File()
	{
		save();
		printf("\ngoodbye %s at %f s\n", no.c_str(), (double) clock() / CLOCKS_PER_SEC);
	}
	bool link(int i, int j)
	{
		// printf("link %d %d\n", i, j);
		Node *ni = &ns[i], *nj = &ns[j];
		if(ni->rs)
			return false;
		if(nj->ls)
			return false;
		if(nj->rm == ni)
			return false;
		ensure(ni->lm != nj);
		ni->rs = nj;
		nj->ls = ni;
		ensure(!ni->lm->ls);
		ni->lm->rm = nj->rm;
		ensure(!nj->rm->rs);
		nj->rm->lm = ni->lm;
		b.push_back(::link(i, j));
		STD::link(i, j);
		return true;
	}
	bool cut(int i)
	{
		// printf("cut %d\n", i);
		Node *ni = &ns[i];
		if(!ni->rs)
			return false;
		Node *nl = ni, *nr = ni;
		while(nl->ls)
			nl = nl->ls;
		while(nr->rs)
			nr = nr->rs;
		Node *nj = ni->rs;
		nl->rm = ni; nr->lm = nj;
		ni->rs = 0; ni->lm = nl;
		nj->ls = 0; nj->rm = nr;
		b.push_back(::cut(i));
		STD::cut(i);
		return true;
	}
	bool query(string s, int k)
	{
		// todo: != 0
		b.push_back(::query(s, k));
		return true;
	}
	void randA(int n, int ma = MAXA)
	{
		a.resize(n + 1);
		f(i, 1, n)
		{
			a[i] = 1;
			while(a[i] < ma && R() % 10 < a[i])
				++a[i];
		}
		ns.clear();
		ns.resize(n + 1);
	}
	void randAG(int n, int ma = MAXA)
	{
		a.resize(n + 1);
		f(i, 1, n)
		{
			a[i] = 1;
			while(a[i] < ma && R() % 100 < a[i])
				++a[i];
		}
		ns.clear();
		ns.resize(n + 1);
	}
	string randStr(int len, int ma = MAXA)
	{
		string ans = "";
		while(len--)
			ans += '1' + R() % ma;
		return ans;
	}
	void randB(int m, int msl, int mk = MAXK, int md = MAXDEL, int ma = MAXA, int qfr = 30, bool k1 = false, bool GG = false)
	{
		STD::init(a);
		int om = m, con = a.size() - 1;
		while(m--)
		{
			if(m % 100 == 0)
				printf("query %d msl %d md %d con %d  \r", m, msl, md, con);
			if(msl <= mk && md == 0)
				break;
			bool ok = false;
			int n = a.size() - 1;
			while(!ok)
				if(R() % 25)
				{
					static int i = 1;
					static int j = 1;
					if(ns[i].rs)
						i = R() % n + 1;
					if(ns[j].ls || ns[j].rm == &ns[i])
						j = R() % n + 1;
					ok = link(i, j);
					if(ok)
						--con;
				}
				else if(R() % qfr)
				{
					if(GG)
					{
						ok = query(string("") + (char) ('1' + R() % ma), 1);
						continue;
					}
					int k = min(mk, (int) (mk * pow((double) R() / RMAX, .1) + 1));
					// printf("k %d\n", k);
					int sl = min(msl, k1 ? R() % 100 : k * k * k * k);
					// printf("k %d\n", k);
					if(sl >= k)
					{
						string s = STD::suggest(k, sl);
						if((int) s.length() >= msl)
							s.resize(msl);
						if((int) s.length() >= pow(k, 1 + (double) R() / RMAX))
						{
							if(R() % 100 == 0)
								s[R() % s.length()] = '1' + R() % ma;
							ok = query(s, k);
							if(ok)
								msl -= s.length();
						}
					}
				}
				else if(md * 11111 > msl && R() % 10 == 0)
				{
					int i = R() % n + 1;
					ok = cut(i);
					if(ok)
					{
						--md;
						++con;
					}
				}
		}
	}
	void dump(ostream &ouf)
	{
		int n = a.size() - 1, m = b.size();
		ensure(n >= 1 && n <= MAXN);
		ensure(m >= 1 && m <= MAXM);
		ouf << n << ' ' << m << '\n';
		f(i, 1, n)
		{
			ensure(a[i] >= 1 && a[i] <= MAXA);
			ouf << a[i] << " \n"[i == n];
		}
		int sl = 0;
		g(i, 0, m)
			if(b[i].op == 1)
			{
				ensure(b[i].i >= 1 && b[i].i <= n);
				ensure(b[i].j >= 1 && b[i].j <= n);
				ensure(b[i].i != b[i].j);
				// ensure(i and j not together);
				// ensure(are head and tail);
				ouf << '1' << ' ' << b[i].i << ' ' << b[i].j << '\n';
			}
			else if(b[i].op == 2)
			{
				ensure(b[i].i >= 1 && b[i].i <= n);
				// ensure(i is not tail);
				ouf << '2' << ' ' << b[i].i << '\n';
			}
			else if(b[i].op == 3)
			{
				ensure(b[i].i >= 1 && b[i].i <= MAXK);
				ensure(b[i].i <= (int) b[i].s.size());
				for(char c: b[i].s)
					ensure(c >= '1' && c <= '6');
				sl += (int) b[i].s.size();
				ensure(sl <= MAXSL);
				ouf << '3' << ' ' << b[i].s << ' ' << b[i].i << '\n';
			}
			else
				ensure(0);
	}
	void dump(const string &fn)
	{
		ofstream ouf(fn);
		dump(ouf);
	}
	void save()
	{
		static string std = "..\\saffah\\std";
		string inf = "../data/" + no + ".in";
		string ouf = "../data/" + no + ".ans";
		dump(inf);
		system((std + " < " + inf + " > " + ouf).c_str());
	}
};

int main()
{
	// m, s, k, d, a
	/*{
		File file(101);
		file.randAG(1000);
		file.randB(2000, 1000, 50, 1000);
	}
	{
		File file(102);
		file.randA(50000);
		file.randB(300000, 2500000, 50, 0, 6, 10);
	}
	{
		File file(103);
		file.randA(100000);
		file.randB(300000, 100000, 50, 1000, 6, 3);
	}
	{
		File file(104);
		file.randA(200000);
		file.randB(300000, 200000, 7, 1000, 6, 2);
	}*/
	{
		File file(1);
		file.randAG(1);
		file.randB(998, 1000, 50, 1000, 6, 30, true, true);
	}
	/*{
		File file(2);
		file.randAG(20);
		file.randB(2000, 1000, 10, 0);
	}
	{
		File file(3);
		file.randAG(150);
		file.randB(2000, 1000, 50, 1000);
	}
	{
		File file(4);
		file.randAG(500);
		file.randB(2000, 1000, 50, 0);
	}
	{
		File file(5);
		file.randAG(1000);
		file.randB(2000, 1000, 50, 1000);
	}
	{
		File file(6);
		file.randA(50000);
		file.randB(300000, 50000, 5, 1000, 6, 2);
	}
	{
		File file(7);
		file.randA(50000, 1);
		file.randB(300000, 50000, 50, 0, 1, 2);
	}
	{
		File file(8);
		file.randA(50000);
		file.randB(300000, 50000, 50, 0, 6, 2);
	}
	{
		File file(9);
		file.randA(50000);
		file.randB(300000, 50000, 50, 1000, 6, 2);
	}
	{
		File file(10);
		file.randA(50000);
		file.randB(300000, 2500000, 50, 0, 6, 10);
	}
	{
		File file(11);
		file.randA(50000);
		file.randB(300000, 2500000, 50, 1000, 6, 10);
	}
	{
		File file(12);
		file.randA(100000);
		file.randB(300000, 100000, 6, 1000, 6, 2);
	}
	{
		File file(13);
		file.randA(100000, 1);
		file.randB(300000, 100000, 50, 0, 1, 3);
	}
	{
		File file(14);
		file.randA(100000);
		file.randB(300000, 100000, 50, 0, 6, 3);
	}
	{
		File file(15);
		file.randA(100000);
		file.randB(300000, 100000, 50, 1000, 6, 3);
	}
	{
		File file(16);
		file.randA(100000);
		file.randB(300000, 5000000, 50, 0);
	}
	{
		File file(17);
		file.randA(100000);
		file.randB(300000, 5000000);
	}
	{
		File file(18);
		file.randA(200000);
		file.randB(500000, 10000000, 1, 0, 6, 10, true);
	}
	{
		File file(19);
		file.randA(200000);
		file.randB(500000, 10000000, 1, 1000, 6, 10, true);
	}
	{
		File file(20);
		file.randA(200000);
		file.randB(300000, 200000, 7, 1000, 6, 2);
	}
	{
		File file(21);
		file.randA(200000, 1);
		file.randB(300000, 200000, 50, 0, 1, 5);
	}
	{
		File file(22);
		file.randA(200000);
		file.randB(300000, 200000, 50, 0, 6, 5);
	}
	{
		File file(23);
		file.randA(200000);
		file.randB(300000, 200000, 50, 1000, 6, 5);
	}
	{
		File file(24);
		file.randA(200000);
		file.randB(300000, 10000000, 50, 0);
	}
	{
		File file(25);
		file.randA(200000);
		file.randB(300000, 10000000);
	}*/
	return 0;
}
