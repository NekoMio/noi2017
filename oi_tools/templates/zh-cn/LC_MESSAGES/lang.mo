��    &      L              |  
   }     �     �     �     �  W   �                    %     2     @     G     U     q     �     �     �     �     �  +   �               1  T   Q  
   �     �     �     �     �     �                 	   #     -     >  >  C     �     �     �     �     �  Y   �                '     4     A     Q     X  $   e     �     �     �     �     �     �  +   �  	        #     <  e   [     �     �  	   �     �     	     !	     %	     2	     6	     C	     G	     T	   Background Case related Content Description Explanation For each input file, you should accomplish an output file named *%s1.out* ~ *%s%d.out*. Hint Input Input Format Memory limit Memory limit: Output Output Format Read from the file *%s.in*. Read from the standard input. Related files: Sample Sample Explanation %s Sample Input %s Sample Output %s See *%(in)s* and *%(ans)s* in the %(path)s. Subtasks This is an interactive problem. This is an output-only problem. This is an output-only problem. There are %d input files named *%s1.in* ~ *%s%d.in*. Time limit Time limit: Time: Write to the file *%s.out*. Write to the standard output. day download files month problem path second(s) user's home path year Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2017-04-30 20:08+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: utf-8
Generated-By: pygettext.py 1.5
 题目背景 非等分测试点 目~~录 题目描述 解释 对于每组输入数据，你需要提交相应的输出文件 *%s1.out* ~ *%s%d.out*。 提示 输入 输入格式 空间限制 空间限制： 输出 输出格式 从文件 *%s.in* 中读入数据。 从标准输入读入数据。 相关文件： 样例 样例%s解释 样例%s输入 样例%s输出 见%(path)s下的 *%(in)s* 与 *%(ans)s*。 子任务 这是一道交互题。 这是一道提交答案题。 这是一道提交答案题，共有%d组输入数据，这些数据命名为 *%s1.in* ~ *%s%d.in*。 时间限制 时间限制： 时间： 输出到文件 *%s.out* 中。 输出到标准输出。 日 下载目录 月 题目目录 秒 选手目录 年 