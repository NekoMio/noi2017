#include <cstdio>
#include <cstring>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)
typedef long long LL;

int _qa[100186], _qb[10000186], _qc[10000186];

int main(){
	memset(_qa, 0x80, sizeof(_qa));
	memset(_qb, 0x80, sizeof(_qb));
	memset(_qc, 0x80, sizeof(_qc));
	int *qa = _qa;
	int *qb = _qb, *qbe = _qb;
	int *qc = _qc, *qce = _qc;
	int n, m, q, px, py, t;
	scanf("%d%d%d%d%d%d", &n, &m, &q, &px, &py, &t);
	int mark = 0;
	g(i, 0, n) scanf("%d", _qa + i);
	std::sort(_qa, _qa + n);
	std::reverse(_qa, _qa + n);
	int oc = 0; bool first = true;
	while(m--){
		// printf("asdf %d %d %d\n", *qa, *qb, *qc);
		int **qp = &qa;
		if(*qb > **qp) qp = &qb;
		if(*qc > **qp) qp = &qc;
		int as = *(*qp)++ + mark;
		mark += q;
		if(++oc == t){
			oc = 0;
			if(first) first = false; else putchar(' ');
			printf("%d", as);
		}
		int bs = (LL) as * px / py;
		*qbe++ = bs - mark; *qce++ = as - bs - mark;
	}
	putchar('\n');
	oc = 0; first = true;
	for(;;){
		int **qp = &qa;
		if(*qb > **qp) qp = &qb;
		if(*qc > **qp) qp = &qc;
		if(++oc == t){
			oc = 0;
			int c = *(*qp)++ + mark;
			if(c < 0) break;
			if(first) first = false; else putchar(' ');
			printf("%d", c);
		}else ++*qp;
	}
	putchar('\n');
	return 0;
}
