#include <cstdio>
#include <algorithm>
#include <queue>

using namespace std;

const int kN = 1e5 + 10;

int n, m, qq, u, v, t;
int a[kN];

void cut(int len, int &l1, int &l2) {
    l1 = 1ll * len * u / v;
    l2 = len - l1;
}

int main() {
    scanf("%d %d %d %d %d %d", &n, &m, &qq, &u, &v, &t);
    for (int i = 1; i <= n; i++) scanf("%d", a+i);
    priority_queue<int> q;
    int added = 0;
    for (int i = 1; i <= n; i++) q.push(a[i]);

    for (int i = 1; i <= m; i++) {
        int len = q.top(); q.pop();
        len += added;
        added += qq;

        int l1, l2;
        cut(len, l1, l2);
        q.push(l1 - added);
        q.push(l2 - added);
        
        if (i % t == 0)
            printf(i != t ? " %d" : "%d", len);
    }
    printf("\n");

    int i = 0;
    while (q.size()) {
        i++;
        int len = q.top(); q.pop();
        len += added;
        
        if (i % t == 0)
            printf(i != t ? " %d" : "%d", len);
    }
    printf("\n");

    return 0;
}