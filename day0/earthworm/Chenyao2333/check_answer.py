#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

def compile_src(src, dst):
    cmd = "g++ %s -o %s" % (src, dst)
    if os.system(cmd) != 0:
        print("Compilation Error")
        sys.exit(-1)

def check(src, ans):
    print("Checking %s" % ans)
    if os.system("%s < %s > tmp.out" % ("./a.out", src)) != 0:
        print("Runtime Error")
        sys.exit(-1)
    if os.system("diff tmp.out %s" % ans) != 0:
        print("Wrong Answer")
        sys.exit(-1)
        
def main():
    compile_src("./brute/earthworm.cpp", "./a.out")
    for i in range(1, 4):
        check("../down/earthworm%s.in" % i, "../down/earthworm%s.ans" % i)
    for i in range(1, 21):
        check("../data/earthworm%s.in" % i, "../data/earthworm%s.ans" % i)

if __name__ == "__main__":
    main()
