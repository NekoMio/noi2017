#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import json

def check(id, n, m, q_min, q_max, v_min, v_max, t, a_min, a_max):
    file = "../data/earthworm%d.in" % id
    print("Checking %s..." % file)
    cmd = ("./a.out %d %d %d %d %d %d %d %d %d < %s" % 
          (n, m, q_min, q_max, v_min, v_max, t, a_min, a_max, file))
    print(cmd)
    if os.system(cmd) != 0:
        print("Error in %s" % file)
        sys.exit(-1)

def main():
    os.system("g++ validator.cpp")
    prob = ""
    with open("../prob.json", 'rb') as f:
        prob = f.read().decode('utf-8')
    prob = json.loads(prob)
    
    for case in prob["data"]:
        args = case["args"]
        if args[5] == 1:
            args[5] = 0
        else:
            args[5] = 200
        check(id = case["cases"][0], n = args[0], m = args[1], t = args[2],
              a_min = 0, a_max = args[3], v_min = 0, v_max = args[4],
              q_min = 0, q_max = args[5]);

if __name__ == "__main__":
    
    main()
