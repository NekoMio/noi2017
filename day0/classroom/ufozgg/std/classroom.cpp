#include<cstdio>
#include<cstring>
#define inf 1e18
#define MAXN 2010
#define MAXV 310
int n,m,v,e;
int c[MAXN],d[MAXN];
double k[MAXN];
int dis[MAXV][MAXV];
double dp[MAXN][MAXN][2],ans=inf;
double min(double c1,double c2)
{
	if(c1<c2)
		return c1;
	return c2;
}
int main()
{
	int i,j,s,a,b,w;
	scanf("%d%d%d%d",&n,&m,&v,&e);
	for(i=1;i<=n;++i)
		scanf("%d",c+i);
	for(i=1;i<=n;++i)
		scanf("%d",d+i);
	for(i=1;i<=n;++i)
		scanf("%lf",k+i);
	memset(dis,60,sizeof(dis));
	for(i=1;i<=v;++i)
		dis[i][i]=0;
	for(i=1;i<=e;++i)
	{
		scanf("%d%d%d",&a,&b,&w);
		if(dis[a][b]>w)
			dis[a][b]=dis[b][a]=w;
	}
	for(s=1;s<=v;++s)
		for(i=1;i<=v;++i)
			for(j=1;j<=v;++j)
				if(dis[i][j]>dis[i][s]+dis[s][j])
					dis[i][j]=dis[i][s]+dis[s][j];
	for(i=0;i<=n;++i)
		for(j=0;j<=m;++j)
			dp[i][j][0]=dp[i][j][1]=inf;
	dp[1][0][0]=0;
	dp[1][1][1]=0;
	for(i=2;i<=n;++i)
		for(j=0;j<=i&&j<=m;++j)
		{
			dp[i][j][0]=min(
				dp[i-1][j][0]+dis[c[i-1]][c[i]],
				dp[i-1][j][1]+k[i-1]*dis[d[i-1]][c[i]]+(1.0-k[i-1])*dis[c[i-1]][c[i]]
				);
			if(j)
				dp[i][j][1]=min(
					dp[i-1][j-1][0]+k[i]*dis[c[i-1]][d[i]]+(1.0-k[i])*dis[c[i-1]][c[i]],
					dp[i-1][j-1][1]+
						k[i]*k[i-1]*dis[d[i]][d[i-1]]+
						k[i]*(1.0-k[i-1])*dis[d[i]][c[i-1]]+
						(1.0-k[i])*k[i-1]*dis[c[i]][d[i-1]]+
						(1.0-k[i])*(1.0-k[i-1])*dis[c[i]][c[i-1]]
					);
		}
	for(i=0;i<=m;++i)
	{
		if(dp[n][i][0]<ans)
			ans=dp[n][i][0];
		if(dp[n][i][1]<ans)
			ans=dp[n][i][1];
	}
	printf("%.2lf\n",ans);
	return 0;
} 
