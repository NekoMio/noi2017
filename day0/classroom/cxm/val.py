import json
import re

inf = 1e100

class InputException(Exception):
	def __init__(self, value = ''):
		self.value = value
	def __str__(self):
		return '[E]At line %d : %s' % (line, repr(self.value))
		
def get_strs(f, num = None, split = ' '):
	global line
	line += 1
	items = f.readline().strip('\n').split(split)
	if num != None and len(items) != num:
		raise InputException('Must contain %d items, but %d get.%s' % (
			num,
			len(items),
			' May contain extra spaces at the end of the line.' if len(items) > num else ''
		))
	return items

def get_ints(f, num = None, split = ' '):
	items = get_strs(f, num, split)
	try:
		return list(map(int, items))
	except Exception as e:
		raise InputException(e)
		
def get_eof(f):
	global line
	line += 1
	extra = f.readline()
	if extra:
		raise InputException('Must get EOF but "%s" get.' % extra)
		
def check_in(item, in_set, name = None):
	if item not in in_set:
		raise InputException('Variable%s must in %s, but %s get.' % (' "' + name + '"' if name else '', repr(in_set), repr(item)))
		
def check_re(item, r, name = None):
	if not r.match(item):
		raise InputException('Variable%s must match "%s", but %s get.' % (' "' + name + '"' if name else '', r.pattern, repr(item)))
		
def check_true(result, info = None):
	if not result:
		raise InputException(info if info else 'Expression must be True but False get.')

def shorter(dist, n):
	for k in range(n):
		for i in range(n):
			for j in range(n):
				if dist[i][j] > dist[i][k] + dist[k][j]:
					return (i, j, k)
	return None

def check(f, args = None):
	first_self_loop = True
	first_repeat_edge = True
	try:
		n, m, v, e = get_ints(f, 4)
		check_in(n, range(1, min(conf['args'][0], args[0] if args else inf) + 1), 'n')
		check_in(m, range(0, min(conf['args'][1], args[1] if args else inf) + 1), 'm')
		check_in(v, range(1, min(conf['args'][2], args[2] if args else inf) + 1), 'v')
		check_in(e, range(1, conf['args'][3] + 1), 'e')
		c = get_ints(f, n)
		for i in range(n):
			check_in(c[i], range(1, v + 1), 'c[%d]' % (i + 1))
		d = get_ints(f, n)
		for i in range(n):
			check_in(d[i], range(1, v + 1), 'd[%d]' % (i + 1))
		k = get_strs(f, n)
		for i in range(n):
			if not args or args[4] == 0:
				check_re(k[i], re.compile(r'^(0\.\d{1,3}|1\.0{1,3}|0|1)$'), 'k[%d]' % (i + 1))
			else:
				check_re(k[i], re.compile(r'^(1\.0{1,3}|1)$'), 'k[%d]' % (i + 1))
		edges = set()
		dist = [[inf] * v for i in range(v)]
		for i in range(v):
			dist[i][i] = 0
		for i in range(e):
			a, b, w = get_ints(f, 3)
			check_in(a, range(1, v + 1), 'a[%d]' % (i + 1))
			check_in(b, range(1, v + 1), 'b[%d]' % (i + 1))
			check_in(w, range(1, conf['args'][4] + 1), 'w[%d]' % (i + 1))
			if (a == b) and first_self_loop:
				print('[W]Self loop detected: a[%d] = b[%d] = %d.' % (i + 1, i + 1, a))
				first_self_loop = False
			if ((a, b) in edges or (b, a) in edges) and first_repeat_edge:
				print('[W]Edge (a[%d], b[%d]) = (%d, %d) showed twice.' % (i + 1, i + 1, a, b))
				first_repeat_edge = False
			edges.add((a, b))
			dist[a - 1][b - 1] = dist[b - 1][a - 1] = min(dist[a - 1][b - 1], w)
		if args and args[3] == 1:
			flag = True
			for i in range(v):
				for j in range(i + 1, v):
					flag &= (dist[i][j] != inf)
			check_true(flag, 'Not all pairs of vertices contain edges.')
			res = shorter(dist, v) or (0, 0, 0)
			check_true(res == (0, 0, 0), 'dist[%d,%d] is longer than dist[%d,%d]+dist[%d,%d]' % (res[0]+1, res[1]+1, res[0]+1, res[2]+1, res[2]+1, res[1]+1))
		get_eof(f)
		print('Check succeeded.')
	except Exception as e:
		print(e)
		print('Check failed.')

if __name__ == '__main__':
	conf = json.loads(open('../prob.json', 'rb').read().decode('utf-8'))
	for i in range(conf['sample count']):
		with open('../down/%s%d.in' % (conf['name'], i + 1), 'r') as f:
			line = 0
			print('Checking sample %d' % (i + 1))
			check(f)
	for datum in conf['data']:
		for case in datum['cases']:
			print('Checking data %d' % case)
			try:
				with open('../data/%s%d.in' % (conf['name'], case), 'r') as f:
					line = 0
					check(f, datum['args'])
			except Exception as e:
				print('[E]Can\'t read input file.')
